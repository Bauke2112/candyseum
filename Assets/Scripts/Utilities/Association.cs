﻿using UnityEngine;
using System.Collections.Generic;

public class Association<type1, type2>
{
    public type1 key;
    public type2 value;

    //Constructors
    public Association(type1 _key, type2 _value)
    {
        this.key = _key;
        this.value = _value;
    }

    public Association()
    {
        
    }
}
