﻿using UnityEngine;
using System.Collections;
using System;

namespace Effects
{
    public static class Animations
    {
        //Sprite Renderer Effects
        public static IEnumerator DecreaseAlpha(this SpriteRenderer renderer, float time, float targetAlpha, Action action = null)
        {
            do
            {
                Color c = renderer.color;
                c.a -= Time.deltaTime / time;
                renderer.color = c;
                yield return new WaitForSeconds(.045f);

                if (c.a <= targetAlpha)
                {
                    c.a = targetAlpha;
                    renderer.color = c;
                    if (action != null) action();
                    yield return null;
                }
            } while (renderer.color.a > targetAlpha);
        }

        public static IEnumerator IncreaseAlpha(this SpriteRenderer renderer, float time, float targetAlpha, Action action = null)
        {
            do
            {
                Color c = renderer.color;
                c.a += Time.deltaTime / time;
                renderer.color = c;
                yield return new WaitForSeconds(.045f);

                if (c.a >= targetAlpha)
                {
                    c.a = targetAlpha;
                    renderer.color = c;
                    if (action != null) action();
                    yield return null;
                }
            } while (renderer.color.a < targetAlpha);
        }

        public static void SetColor(this SpriteRenderer renderer, Color newColor)
        {
            renderer.color = newColor;
        }

        public static void SetAlpha(this SpriteRenderer renderer, float newAlpha)
        {
            Color c = renderer.color;
            c.a = newAlpha;
            renderer.color = c;
        }

        //Camera Renderer
        
    }
}
