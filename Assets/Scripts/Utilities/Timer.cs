﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Timers
{
    public class Timer
    {
        private static List<Timer> activeTimers;
        private static GameObject initGameobject;

        private static void InitIfNeed()
        {
            if(initGameobject == null)
            {
                initGameobject = new GameObject("InitTimer_GameObject");
                activeTimers = new List<Timer>();
            }
        }

        public class TimerHook : MonoBehaviour
        {
            public Action onUpdate;

            private void Update()
            {
                if (onUpdate != null) onUpdate();    
            }
        }

        public static Timer CreateTimer(Action action, float time, string timerName = null)
        {
            InitIfNeed();
            GameObject timerObject = new GameObject("Timer Object", typeof(TimerHook));
            Timer timer = new Timer(time, action, timerObject, timerName);
            activeTimers.Add(timer);

            timerObject.GetComponent<TimerHook>().onUpdate = timer.Update;

            return timer;
        }

        private static void RemoveTimer(Timer timer)
        {
            InitIfNeed();
            activeTimers.Remove(timer);
        }

        public static void StopTimer(string timerName)
        {
            for(int i = 0; i < activeTimers.Count; i++)
            {
                if(activeTimers[i].timerName == timerName)
                {
                    activeTimers[i].DestroySelf();
                    i--;
                }
            }
        }

        public static string GetTimerTime(string timerName)
        {
            for (int i = 0; i < activeTimers.Count; i++)
            {
                if (activeTimers[i].timerName == timerName)
                {
                    return activeTimers[i].time.ToString();
                }
            }

            return null;
        }

        private float time;
        private Action action;
        private bool isDestroyed;
        private GameObject gameObject;
        private string timerName;

        private Timer(float timer, Action action, GameObject gameObject, string timerName)
        {
            this.time = timer;
            this.action = action;
            this.isDestroyed = false;
            this.timerName = timerName;
            this.gameObject = gameObject;
        }

        public void Update()
        {
            if (isDestroyed)
                return;

            time -= Time.deltaTime;
            if (time <= 0)
            {
                action();
                DestroySelf();
            }
        }

        public void DestroySelf()
        {
            isDestroyed = true;
            UnityEngine.Object.Destroy(this.gameObject);
            RemoveTimer(this);
        }
    }
}
