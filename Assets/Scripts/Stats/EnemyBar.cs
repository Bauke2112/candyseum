﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyBar : MonoBehaviour {

    public Image bar;

    private EnemySpawnAI enemyStats;
    private BossAI bossStats;

    private void Awake()
    {
        enemyStats = this.GetComponentInParent<EnemySpawnAI>();
        bossStats = this.GetComponentInParent<BossAI>();

        if (enemyStats == null || bossStats == null)
            return;

        this.GetComponent<Canvas>().worldCamera = Camera.main;
    }

    private void Update()
    {
        if(enemyStats == null)
        {
            bar.fillAmount = bossStats.GetFillAmount();
        }else if(bossStats == null)
        {
            bar.fillAmount = enemyStats.GetFillAmount();
        }
    }
}
