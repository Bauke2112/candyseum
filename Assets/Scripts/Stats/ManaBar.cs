﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ManaBar : MonoBehaviour {

    public Image manaBar;
    public PlayerStats targetStats;

    public TextMeshProUGUI text;

    private void Awake()
    {
        if (targetStats == null)
            return;
    }

    private void Update()
    {
        manaBar.fillAmount = targetStats.GetManaFillAmount();
        text.text = Mathf.FloorToInt(targetStats.mana) + " / " + targetStats.maxMana;
    }
}
