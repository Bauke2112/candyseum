﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBar : MonoBehaviour {

    public Image healthBar;
    public PlayerStats targetStats;

    public TextMeshProUGUI text;

    private void Awake()
    {
        if (targetStats == null)
            return;
    }

    private void Update()
    {
        healthBar.fillAmount = targetStats.GetHealthFillAmount();
        text.text = Mathf.FloorToInt(targetStats.hp) + " / " + targetStats.maxHp;
    }
}
