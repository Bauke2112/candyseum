﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class SettingsData {

    public float volume;
    public int graphics;
    public int resolution;
    public bool fullScreen;
    public bool hideInv;
    public bool inGameTutorial;

    public SettingsData(Slider volumeSlide, TMP_Dropdown graphicsDrop, TMP_Dropdown resolutionDrop, Toggle fullScreenToggle, Toggle hideInvToggle, Toggle inGameToggle)
    {
        volume = volumeSlide.value;
        graphics = graphicsDrop.value;
        resolution = resolutionDrop.value;
        fullScreen = fullScreenToggle.isOn;
        hideInv = hideInvToggle.isOn;
        inGameTutorial = inGameToggle.isOn;
    }
}
