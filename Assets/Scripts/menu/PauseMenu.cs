﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour {

    public static PauseMenu instance;
    public EventSystem eventSystem;

    public GameObject defaultSystemButton;

    public GameObject shopOwner;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
    }

    public GameObject pauseMenu;
    public bool isOpen;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || InputManager.Start_Button())
        {
            if(shopOwner.activeInHierarchy)
            {
                if(ShopOwner.instance.isOpen)
                {
                    ShopOwner.instance.Close();
                }else
                {
                    isOpen = !isOpen;
                }
            }else
            {
                isOpen = !isOpen;
            }
        }

        if(isOpen)
        {
            eventSystem.firstSelectedGameObject = defaultSystemButton;
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
        }else
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void Resume()
    {
        isOpen = false;
    }

    public void Back()
    {
        isOpen = false;
        StartCoroutine(Transitions.instance.LoadScene("MainMenu"));
    }

    public void Exit()
    {
        HUDController.inGameTutorial = true;
        Application.Quit();
    }
}
