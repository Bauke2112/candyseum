﻿using UnityEngine;

public class MenuManager : MonoBehaviour {

    public void Menu(){
        StartCoroutine(Transitions.instance.LoadScene("MainMenu"));
    }

	public void Play(string scene){
        StartCoroutine(Transitions.instance.LoadScene(scene));
    }

	public void Credits(){
        StartCoroutine(Transitions.instance.LoadScene("Credits"));
    }

    public void Settings()
    {
        StartCoroutine(Transitions.instance.LoadScene("Settings"));
    }

    public void LoadScene(string name)
    {
        StartCoroutine(Transitions.instance.LoadScene(name));
    }

    public void PlayClickSound(AudioClip clip)
    {
        this.GetComponent<AudioSource>().clip = clip;
        this.GetComponent<AudioSource>().Play();
    }

	public void Exit(){
        HUDController.inGameTutorial = true;
        Application.Quit();
	}
}
