﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;
using TMPro;

public static class SettingSaveSystem {

    public static void SaveSettings(Slider volumeSlide, TMP_Dropdown graphicsDrop, TMP_Dropdown resolutionDrop, Toggle fullScreenToggle, Toggle hideInvToggle, Toggle inGame)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/settings.set";
        FileStream stream = new FileStream(path, FileMode.Create);

        SettingsData data = new SettingsData(volumeSlide, graphicsDrop, resolutionDrop, fullScreenToggle, hideInvToggle, inGame);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static SettingsData LoadSettings()
    {
        string path = Application.persistentDataPath + "/settings.set";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SettingsData data = (SettingsData)formatter.Deserialize(stream);
            stream.Close();
            return data;
        } else
        {
            Debug.Log("File not found");
            return null;
        }
    }
}
