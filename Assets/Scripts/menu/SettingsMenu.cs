﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour {

    public TMP_Dropdown resolutionDropdown;
    public AudioMixer mainMixer;

    Resolution[] resolutions;

    private void Start()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();
        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = new Resolution();

        if (!SettingsMenuController.instance.isLoading)
            resolution = resolutions[resolutionIndex];

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetFullscrean(bool isFullscreen)
    {
        if (!SettingsMenuController.instance.isLoading)
            Screen.fullScreen = isFullscreen;
    }

    public void SetHideInventory(bool hideInv)
    {
        if (!SettingsMenuController.instance.isLoading)
            HUDController.hideInventory = hideInv;
    }

    public void SetQuality(int qualityIndex)
    {
        if (!SettingsMenuController.instance.isLoading)
            QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetVolume(float volume)
    {
        if (!SettingsMenuController.instance.isLoading)
            mainMixer.SetFloat("Volume", volume);
    }

    public void SetInGameTutorial(bool value)
    {
        if (!SettingsMenuController.instance.isLoading)
            HUDController.inGameTutorial = value;
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(Transitions.instance.LoadScene(sceneName));
    }

    public void PlayClickSound(AudioClip clip)
    {
        this.GetComponent<AudioSource>().clip = clip;
        this.GetComponent<AudioSource>().Play();
    }
}
