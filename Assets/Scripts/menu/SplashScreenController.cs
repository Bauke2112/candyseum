﻿using UnityEngine;

public class SplashScreenController : MonoBehaviour {

    public float logoTime = 3.5f;
    public string nextSceneName;

    private float timer;

    private void Start()
    {
        timer = logoTime;
    }

    private void Update()
    {
        if(timer <= 0)
        {
            FadeOut();
        }else
        {
            timer -= Time.deltaTime;
        }
    }

    void FadeOut()
    {
        StartCoroutine(Transitions.instance.LoadScene(nextSceneName));
    }
}
