﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuController : MonoBehaviour {

    public static SettingsMenuController instance;

    private void Awake()
    {
        instance = this;
    }

    public GameObject[] settingsOptions;
    public int defaultIndex = 0;
    public int index;

    //0 -- graphics
    //1 -- audio
    //2 -- gameplay

    public Slider volumeSlide;
    public TMP_Dropdown graphicsDrop;
    public TMP_Dropdown resolutionDrop;
    public Toggle fullScreenToggle;
    public Toggle hideInvToggle;
    public Toggle inGameToggle;

    public bool isLoading;

    public void SaveContent()
    {
        SettingSaveSystem.SaveSettings(volumeSlide, graphicsDrop, resolutionDrop, fullScreenToggle, hideInvToggle, inGameToggle);
    }


    private void Start()
    {
        SettingsData data = SettingSaveSystem.LoadSettings();

        if (data == null)
            return;

        isLoading = true;

        volumeSlide.value = data.volume;
        graphicsDrop.value = data.graphics;
        resolutionDrop.value = data.resolution;
        fullScreenToggle.isOn = data.fullScreen;
        hideInvToggle.isOn = data.hideInv;

        if (data.inGameTutorial != HUDController.inGameTutorial)
            inGameToggle.isOn = HUDController.inGameTutorial;
        else
            inGameToggle.isOn = data.inGameTutorial;

        isLoading = false;

        foreach (GameObject g in settingsOptions)
        {
            g.SetActive(false);
        }

        index = defaultIndex;
        settingsOptions[index].SetActive(true);
    }

    public void ChangeIndex(int newIndex)
    {
        index = newIndex;
        ShowSettings();
    }

    void ShowSettings()
    {
        foreach (GameObject g in settingsOptions)
        {
            g.SetActive(false);
        }

        settingsOptions[index].SetActive(true);
    }
}
