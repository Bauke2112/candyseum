﻿using UnityEngine;
using UnityEngine.UI;

/* Projeto AllStars
 * Feito Por Gabriel Pasquale R. Scavone
 * 
 * 14/04/18
 * 
 */

public class menu : MonoBehaviour {

	public GameObject pausemenu; //objeto do menu

    public GameObject Player1, Player2, Player3, Player4; //criando as variaveis de quem e o player

    public Text nunPlayer;
    public int numeroDePlayers;
    public static int numeroDePlayersForOthers;

    void Start()
    {

        numeroDePlayers = 1;

    }

    // Update is called once per frame
    void Update () {

        numeroDePlayersForOthers = numeroDePlayers;

		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			pausemenu.SetActive (!pausemenu.activeSelf);   //abrir/fechar o menu
			if (Time.timeScale == 1) {        //verificar se o tempo esta passando
                Time.timeScale = 0;      //setar para o tempo nao passar
			} 
			else 
			{
				Time.timeScale = 1;      // se o tempo nao estiver passando, setar novamente para ele passar
			}
		}


        nunPlayer.text = "" + numeroDePlayers + " Players";
        #region                                                                 
        if (numeroDePlayers == 1)                   //verifica o numero de players e so deixa ativo o numero correspondente
        {

            Player2.SetActive(false);
            Player3.SetActive(false);
            Player4.SetActive(false);

        }
        if (numeroDePlayers == 2)
        {

            Player2.SetActive(true);
            Player3.SetActive(false);
            Player4.SetActive(false);

        }
        if (numeroDePlayers == 3)
        {

            Player2.SetActive(true);
            Player3.SetActive(true);
            Player4.SetActive(false);

        }
        if (numeroDePlayers == 4)
        {

            Player2.SetActive(true);
            Player3.SetActive(true);
            Player4.SetActive(true);

        }

        #endregion
    }


	public void desPauseByButton()          //despausar pelo botao resume(codigos a baixo iguais aos de cima)
	{
		pausemenu.SetActive (!pausemenu.activeSelf);        
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
		} 
		else 
		{
			Time.timeScale = 1;
		}
	}

    public void plusNunDePlayers()
    {

        numeroDePlayers += 1;                       //adiciona 1 a vriavel que mostra o numero de players jogando
        if(numeroDePlayers >= 5)        
        {

            numeroDePlayers = 1;                    //se o numero de players for maior que 4, volta a 1

        }

    }

    public void minusNunDePlayers()
    {

        numeroDePlayers -= 1;                       //retira um do numero de players jogando
        if(numeroDePlayers <= 0)                    
        {

            numeroDePlayers = 4;                    //se o numero de players for menor que 1, volta-se a 4

        }


    }
}
