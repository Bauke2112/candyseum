﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectMenu : MonoBehaviour {

    public List<GameObject> activeSelectors;
    public List<GameObject> deactiveSelectors;

    private void Start()
    {
        foreach(GameObject i in activeSelectors)
        {
            if(i.activeInHierarchy)
            {
                return;
            }else
            {
                activeSelectors.Remove(i);
                deactiveSelectors.Add(i);
            }
        }
    }

    public void NextScene()
    {
        StartCoroutine(Transitions.instance.LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void LastScene()
    {
        StartCoroutine(Transitions.instance.LoadScene(SceneManager.GetActiveScene().buildIndex - 1));
    }

    public void LoadScene(string name)
    {
        StartCoroutine(Transitions.instance.LoadScene(name));
    }

    private void Update()
    {
        AddPlayerSelector();
    }

    public void AddPlayerSelector()
    {
        if(Input.GetKeyDown(KeyCode.A) || InputManager.GUI_B_Button())
        {
            if (deactiveSelectors.Count == 0)
                return;

            activeSelectors.Add(deactiveSelectors[0]);
            deactiveSelectors[0].SetActive(true);
            deactiveSelectors.Remove(deactiveSelectors[0]);
            return;
        }
    }

    public void PlayClickSound(AudioClip clip)
    {
        this.GetComponent<AudioSource>().clip = clip;
        this.GetComponent<AudioSource>().Play();
    }
}
