﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class MenuController : MonoBehaviour {

    public static MenuController instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
    }

    public GameObject cursor;

    public List<string> connectedControllers = new List<string>();
    public List<MouseCursor> cursors = new List<MouseCursor>();

    private void Start()
    {
        connectedControllers = null;
        connectedControllers = Input.GetJoystickNames().ToList();

        SpawnCursors();
    }

    private void SpawnCursors()
    {
        foreach(string controller in connectedControllers)
        {
            var c = Instantiate(cursor, Vector2.zero, Quaternion.identity, null);
            c.GetComponent<MouseCursor>().number = -10;

            cursors.Add(c.GetComponent<MouseCursor>());
        }
    }
}
