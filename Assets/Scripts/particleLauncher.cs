﻿using UnityEngine;
using System.Collections.Generic;

public class particleLauncher : MonoBehaviour {

    public float Timer;                                                         //Tempo que o lancachamas ficara ligado
    public ParticleSystem ParticleLauncher;                                     //ParticleSystem do LancaChamas
    public float ReloadTimeMultiplier;                                          //velocidade em que o Timer icrementa
    public float MaxFuel;                                                       //Limite de combustivel do LancaChamas    

    // Update is called once per frame
    private void Awake()
    {
        this.transform.Rotate(0, 90f, 0);
    }
    void Update () {

        //this.transform.parent = GameObject.Find("Player").transform;
        if (Input.GetButton("Fire1"))
        {
            if (Timer >= 0)
            {
                Timer -= Time.deltaTime;                                            
                ParticleLauncher.Emit(1);                                           //liga o emissor do particle system na proporcao de 1
                
            }
        }
        else if (Timer < MaxFuel)
        {
            Timer += ReloadTimeMultiplier * (Time.deltaTime);                       //Incrementa o Timer
        }   
	}
}
