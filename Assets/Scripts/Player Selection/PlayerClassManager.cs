﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class PlayerClassManager : MonoBehaviour
{
    public static PlayerClassManager instance;

    public List<PlayerClass> classes = new List<PlayerClass>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if(SceneManager.GetActiveScene().name == "Game")
            Cursor.visible = false;
        else
            Cursor.visible = true;
    }
}

[System.Serializable]
public class PlayerClass
{
    public string name;

    public float maxHp;
    public float maxMana;
    public float maxStamina;
    public float moveSpeed = 5f;

    public float ultimateManaUsage = 50f;
    public float ultimateDuration = 40f;

    [TextArea]public string ultimateDescription;

    public PlayerType type;
}