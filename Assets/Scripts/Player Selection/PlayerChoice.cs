﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerChoice : MonoBehaviour {

    public Toggle[] P1toggles;
    public Toggle[] P2toggles;
    public Toggle[] P3toggles;
    public Toggle[] P4toggles;

    void Start () {
        if (PlayerPrefs.HasKey("Player1") == true)
        {
            Debug.Log("P1S " + PlayerPrefs.GetInt("Player1"));
            P1toggles[PlayerPrefs.GetInt("Player1")].isOn = true;
        }
        if (PlayerPrefs.HasKey("Player2") == true)
        {
            Debug.Log("P2S " + PlayerPrefs.GetInt("Player2"));
            P2toggles[PlayerPrefs.GetInt("Player2")].isOn = true;
        }
        if (PlayerPrefs.HasKey("Player3") == true)
        {
            Debug.Log("P3S " + PlayerPrefs.GetInt("Player3"));
            P3toggles[PlayerPrefs.GetInt("Player3")].isOn = true;
        }
        if (PlayerPrefs.HasKey("Player4") == true)
        {
            Debug.Log("P4S " + PlayerPrefs.GetInt("Player4"));
            P4toggles[PlayerPrefs.GetInt("Player4")].isOn = true;
        }
    }



    public void Player1choice()
    {
        if (P1toggles[0].isOn == true)
        {
            PlayerPrefs.SetInt("Player1", 0);

        }else if (P1toggles[1].isOn == true)
        {
            PlayerPrefs.SetInt("Player1", 1);

        }else if (P1toggles[2].isOn == true)
        {
            PlayerPrefs.SetInt("Player1", 2);

        }else if (P1toggles[3].isOn == true)
        {
            PlayerPrefs.SetInt("Player1", 3);
        }
        Debug.Log("P1 " + PlayerPrefs.GetInt("Player1"));
    }

    public void Player2choice()
    {
        if (P2toggles[0].isOn == true)
        {
            PlayerPrefs.SetInt("Player2", 0);
        }
        else if (P2toggles[1].isOn == true)
        {
            PlayerPrefs.SetInt("Player2", 1);
        }
        else if (P2toggles[2].isOn == true)
        {
            PlayerPrefs.SetInt("Player2", 2);
        }
        else if (P2toggles[3].isOn == true)
        {
            PlayerPrefs.SetInt("Player2", 3);
        }
        Debug.Log("P2 " + PlayerPrefs.GetInt("Player2"));
    }

    public void Player3choice()
    {
        if (P3toggles[0].isOn == true)
        {
            PlayerPrefs.SetInt("Player3", 0);
        }
        else if (P3toggles[1].isOn == true)
        {
            PlayerPrefs.SetInt("Player3", 1);
        }
        else if (P3toggles[2].isOn == true)
        {
            PlayerPrefs.SetInt("Player3", 2);
        }
        else if (P3toggles[3].isOn == true)
        {
            PlayerPrefs.SetInt("Player3", 3);
        }
        Debug.Log("P3 " + PlayerPrefs.GetInt("Player3"));
    }

    public void Player4choice()
    {
        if (P4toggles[0].isOn == true)
        {
            PlayerPrefs.SetInt("Player4", 0);
        }
        else if (P4toggles[1].isOn == true)
        {
            PlayerPrefs.SetInt("Player4", 1);
        }
        else if (P4toggles[2].isOn == true)
        {
            PlayerPrefs.SetInt("Player4", 2);
        }
        else if (P4toggles[3].isOn == true)
        {
            PlayerPrefs.SetInt("Player4", 3);
        }
        Debug.Log("P4 " + PlayerPrefs.GetInt("Player4"));
    }
}
