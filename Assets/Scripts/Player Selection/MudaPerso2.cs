﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MudaPerso2 : MonoBehaviour
{
    public Scrollbar ScrollBar;
    public int vezesC = 0;
    public string ScrollBarPlayer;
    public GameObject buttonUp, buttonDown;

    void Awake()
    {
        if (PlayerPrefs.HasKey(ScrollBarPlayer) == true)        
            ScrollBar.value = PlayerPrefs.GetFloat(ScrollBarPlayer);
        else
            ScrollBar.value = 1;
        
    }
 
    void Update()
    {
        PlayerPrefs.SetFloat(ScrollBarPlayer, ScrollBar.value);
        
        if (ScrollBar.value <= 0.3F)
            buttonDown.SetActive(false);
        else
            buttonDown.SetActive(true);
        if (ScrollBar.value >= 0.99f)
            buttonUp.SetActive(false);
        else
            buttonUp.SetActive(true);
    }

    public void MudaPersoNaImagem()
    {
        ScrollBar.value -= 0.3f;

    }

    public void MudaPersoNaImagemPraCima()
    {
        ScrollBar.value += 0.3f;

    }
}