﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;

public class ShopController : MonoBehaviour {

    public static ShopController instance;

    private void Awake()
    {
        instance = this;
    }

    //public int playerNumber;
    public TextMeshProUGUI playerGold;

    [HideInInspector]public Inventory curInv;

    private List<MouseCursor> curCursors = new List<MouseCursor>();

    public GameObject shopObject;

    /*private void Start()
    {
        switch(playerNumber)
        {
            case 1:
                curInv = MultplayerController.instance.players[0].GetComponent<Inventory>();
                break;

            case 2:
                curInv = MultplayerController.instance.players[1].GetComponent<Inventory>();
                break;

            case 3:
                curInv = MultplayerController.instance.players[2].GetComponent<Inventory>();
                break;

            case 4:
                curInv = MultplayerController.instance.players[3].GetComponent<Inventory>();
                break;
        }
    }*/

    private void Update()
    {
        playerGold.text = "$" + PlayerGold.instance.goldQuant.ToString();
    }

    public void Close()
    {
        ShopOwner.instance.isOpen = false;
        Debug.Log("just close");
        Time.timeScale = 1;
        shopObject.SetActive(false);
    }

    public void GoToNextWave()
    {
        ShopOwner.instance.isOpen = false;
        Debug.Log("close and next wave");
        Time.timeScale = 1;
        Spawner.instance.CloseStore();
        shopObject.SetActive(false);
    }

    public void BuyItem(Things item)
    {
        if(PlayerGold.instance.goldQuant >= item.price)
        {
            if(curInv.AddItem(item))
            {
                PlayerGold.instance.goldQuant -= item.price;
            }else
            {
                return;
            }
        }
    }
}
