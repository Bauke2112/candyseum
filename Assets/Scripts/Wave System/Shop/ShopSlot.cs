﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopSlot : MonoBehaviour {

    public Things itemToSell;

    [Header("UI Stuff")]
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI itemPrice;
    public Image itemIcon;

    private void Start()
    {
        itemIcon.sprite = itemToSell.image;

        itemName.text = itemToSell.name;
        itemPrice.text = "$" + itemToSell.price.ToString();
    }
}
