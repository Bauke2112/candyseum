﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ShopOwner : MonoBehaviour {

    public static ShopOwner instance;
    public EventSystem eventSystem;

    public GameObject defaultSystemButton;

    public string keyboardKey = "Press E to open";
    public string joystickKey = "Press A to open";

    public bool isOpen;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);

        eventSystem = FindObjectOfType<EventSystem>();
    }

    public GameObject shopObject;
    public Canvas HUD;

    public GameObject textToDisplay;

    private void Start()
    {
        HUD = GameObject.FindGameObjectWithTag("Shop").GetComponent<Canvas>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(MultplayerController.instance.connectedControllers.Count != 0)
        {
            textToDisplay.GetComponentInChildren<TextMeshProUGUI>().text = joystickKey;
        }else
        {
            textToDisplay.GetComponentInChildren<TextMeshProUGUI>().text = keyboardKey;
        }

        //textToDisplay.SetActive(true);

        if (collision.gameObject.CompareTag("Player") && (Input.GetKeyDown(KeyCode.E) || InputManager.A_Button(collision.GetComponent<Player>().number)))
        {
            OpenShop(collision.GetComponent<Player>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //textToDisplay.SetActive(false);
    }

    public void OpenShop(Player p)
    {
        isOpen = true;
        eventSystem.firstSelectedGameObject = defaultSystemButton;

        shopObject.SetActive(true);
        shopObject.transform.localPosition = Vector3.zero;

        shopObject.GetComponent<ShopController>().curInv = p.GetComponent<Inventory>();
        Time.timeScale = 0;
    }

    public void Close()
    {
        isOpen = false;
        Debug.Log("just close");
        Time.timeScale = 1;
        shopObject.SetActive(false);
    }
}
