﻿using System.Collections.Generic;
using UnityEngine;

public class ShopSlotsController : MonoBehaviour {

    public List<Things> itensToSell = new List<Things>();
    public GameObject slot;

    private void Start()
    {
        foreach(Things t in itensToSell)
        {
            GameObject go =Instantiate(slot, this.transform.position, Quaternion.identity, this.transform);
            go.GetComponent<ShopSlot>().itemToSell = t;
        }
    }
}
