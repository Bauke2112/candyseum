﻿using UnityEngine;
using UnityEngine.UI;

public class CurWaveInfo : MonoBehaviour {

    public Spawner spawner;

    [Header("Images")]
    public Image[] slots;
    public Image bossSlot;

    private void Awake()
    {
        if (spawner == null)
            return;
    }

    private void Start()
    {
        foreach (Image slot in slots)
        {
            slot.fillAmount = 0;
        }

        bossSlot.fillAmount = 0;
    }

    private void Update()
    {
       if (spawner.currentWaveNumber == 1)
        {
            slots[0].fillAmount = 1;
            slots[1].fillAmount = 0;
            slots[2].fillAmount = 0;
            slots[3].fillAmount = 0;
            slots[4].fillAmount = 0;
            bossSlot.fillAmount = 0;
        }
        else if (spawner.currentWaveNumber == 2)
        {
            slots[1].fillAmount = 1;
        }
        else if (spawner.currentWaveNumber == 3)
        {
            slots[2].fillAmount = 1;
        }
        else if (spawner.currentWaveNumber == 4)
        {
            slots[3].fillAmount = 1;
        }
        else if (spawner.currentWaveNumber == 5)
        {
            slots[4].fillAmount = 1;
        }
        else if (spawner.currentWaveNumber == 6)
        {
            bossSlot.fillAmount = 1;
        }
    }
}
