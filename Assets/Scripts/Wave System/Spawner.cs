﻿using UnityEngine;
using TMPro;
using System;
using DG.Tweening;

public class Spawner : MonoBehaviour {

    public static Spawner instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
    }

    [Header("HUD")]
    public TextMeshProUGUI roundInfo;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI enemiesInfo;
    public TextMeshProUGUI nextWaveText;
    public InGameTutorial inGameTutorialObject;

    private DOTweenAnimation nextWaveTextAnim;
    private Animator nexWaveTextAnimator;

    private int score = 0;
    private int regulator = 1;

    [Header("Waves")]
    public Wave[] waves;
    public EnemySpawnAI[] enemies;
    public GameObject[] bosses;

    [Header("Spawner")]
    public float spawnRadius = 25f;
    public float secureRadius = 4f;
    public int spawnPointX;
    public int spawnPointY;
    public float[] spawnMargin;
    public float timeToWaitUntilBoss = 2;

    private float bossTimer;

    [Header("Shop")]
    public GameObject shopOwner;
    public GameObject PointerObject;

    private Wave currentWave;
    public int currentWaveNumber;

    [HideInInspector]public int currentRoundNumber = 1;
    [HideInInspector] public int maxEnemies;

    private int enemiesRemaningToSpawn;
    [HideInInspector]public int enemiesRemaningAlive;
    private float nextSpawnTime;
    public static int ActualRound;

    private bool canSpawnBoss;
    private BossAI curBoss;
    //private GameObject shop;

    private void Start()
    {
        nextWaveTextAnim = nextWaveText.GetComponent<DOTweenAnimation>();
        nexWaveTextAnimator = nextWaveText.gameObject.GetComponent<Animator>();

        //nextWaveText.gameObject.SetActive(false);
        //NextWave();
        inGameTutorialObject.InGameTutorialFinished += OnTutorialEnded;
    }

    private void Update()
    {
        ScoresCounter();
        ActualRound = currentRoundNumber;
        roundInfo.text = "Round: " + currentRoundNumber.ToString();
        //waveInfo.text = "Wave: " + currentWaveNumber.ToString();
        EnemiesText();

        if (canSpawnBoss)
        {
            if (bossTimer <= 0)
            {
                canSpawnBoss = false;
                Debug.Log(currentWaveNumber + " is boss wave!");

                int index;
                index = UnityEngine.Random.Range(0, bosses.Length);

                Vector2 pos;
                pos = Vector2.zero;

                BossAI _b = Instantiate(bosses[index], pos, Quaternion.identity, this.transform).GetComponent<BossAI>();
                curBoss = _b;
                _b.OnDeath += OnBossDeath;
            }
            else
            {
                bossTimer -= Time.deltaTime;
            }
        }

        if (enemiesRemaningToSpawn > 0 && Time.time > nextSpawnTime)
        {
            enemiesRemaningToSpawn--;
            nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

            int index;
            index = UnityEngine.Random.Range(0, enemies.Length);


            spawnPointX = UnityEngine.Random.Range(0, spawnMargin.Length);
            spawnPointY = UnityEngine.Random.Range(0, spawnMargin.Length);
            spawnMargin[0] = UnityEngine.Random.Range(spawnRadius, secureRadius);
            spawnMargin[1] = UnityEngine.Random.Range(-spawnRadius, -secureRadius);
            spawnMargin[2] = UnityEngine.Random.Range(spawnRadius, secureRadius);
            spawnMargin[3] = UnityEngine.Random.Range(-spawnRadius, -secureRadius);

            Vector2 pos;
            pos = new Vector2(spawnMargin[spawnPointX], spawnMargin[spawnPointY]);

            EnemySpawnAI _e = Instantiate(enemies[index], pos, Quaternion.identity, this.transform);
            _e.OnDeath += OnEnemyDeath;
        }
    }

    private void OnTutorialEnded()
    {
        NextWave();
    }

    void EnemiesText()
    {
        if (currentWaveNumber == 0)
        {
            enemiesInfo.text = "Tutorial";
        }
        else if(!waves[currentWaveNumber - 1].bossWave && !waves[currentWaveNumber - 1].shopWave)
        {
            enemiesInfo.text = "Enemies: " + enemiesRemaningAlive.ToString();
        } else if(waves[currentWaveNumber - 1].bossWave)
        {
            if (curBoss == null)
                return;

            enemiesInfo.text = curBoss.hp.ToString() + "/" + curBoss.bossAIFile.maxHp.ToString(); 
        }
        else if (waves[currentWaveNumber - 1].shopWave)
        {
            enemiesInfo.text = "Shop Wave!";
        }
    }

    void ScoresCounter()
    {
        if (PlayerPrefs.HasKey("HighRound") == true)
        {
            if (ActualRound >= PlayerPrefs.GetInt("HighRound"))
                PlayerPrefs.SetInt("HighRound", ActualRound);

        }
        else
        {
            PlayerPrefs.SetInt("HighRound", ActualRound);
        }

        //score += ActualRound * currentWaveNumber;
        //Debug.Log(string.Format("Estasts: Wave {0} , Regulador: {1} ", currentWaveNumber, regulator));
        #region
        score += EnemySpawnAI.addScore;
        scoreText.text = string.Format("Score: {0} / {1}", score, PlayerPrefs.GetInt("HighScore"));
        EnemySpawnAI.addScore = 0;
        if (currentWaveNumber == regulator || currentWaveNumber == 1 && regulator == 8)
        {
            score += (ActualRound * currentWaveNumber) * 2;
            regulator += 1;
            if (regulator >= 9)
                regulator = 1;
        }
        //Debug.Log(string.Format("Estasts: Round {0} , Score: {1}, Enemies {2}, Tempo de jogo {3} , DELTATIME {4} ", PlayerPrefs.GetInt("HighRound"), score, PlayerPrefs.GetInt("EnemiesKilled"), PlayerPrefs.GetFloat("TimePlayed"), Time.deltaTime));

        if (PlayerPrefs.HasKey("HighScore") == true)
        {
            if (score >= PlayerPrefs.GetInt("HighScore"))
                PlayerPrefs.SetInt("HighScore", score);

        }
        else
            PlayerPrefs.SetInt("HighScore", score);
        #endregion


        if (PlayerPrefs.HasKey("TimePlayed") == true)
            PlayerPrefs.SetFloat("TimePlayed", PlayerPrefs.GetFloat("TimePlayed") + Time.deltaTime);

        else
            PlayerPrefs.SetFloat("TimePlayed", 0f);


        if (PlayerPrefs.GetInt("Player1") == 0)
       {
            if (PlayerPrefs.HasKey("WarriorTime") == true)
                PlayerPrefs.SetFloat("WarriorTime", PlayerPrefs.GetFloat("WarriorTime") + Time.deltaTime);
            else            
                PlayerPrefs.SetFloat("WarriorTime", 0);

        }
        else if (PlayerPrefs.GetInt("Player1") == 1)
        {
            if (PlayerPrefs.HasKey("ArcherTime") == true)
                PlayerPrefs.SetFloat("ArcherTime", PlayerPrefs.GetFloat("ArcherTime") + Time.deltaTime);
            else
                PlayerPrefs.SetFloat("ArcherTime", 0);
        }
        else if (PlayerPrefs.GetInt("Player1") == 2)
        {
            if (PlayerPrefs.HasKey("MageTime") == true)
                PlayerPrefs.SetFloat("MageTime", PlayerPrefs.GetFloat("MageTime") + Time.deltaTime);
            else
                PlayerPrefs.SetFloat("MageTime", 0);
        }
        else if (PlayerPrefs.GetInt("Player1") == 3)
        {
            if (PlayerPrefs.HasKey("TankTime") == true)
                PlayerPrefs.SetFloat("TankTime", PlayerPrefs.GetFloat("TankTime") + Time.deltaTime);
            else
                PlayerPrefs.SetFloat("TankTime", 0);
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, spawnRadius);
    }

    public void CloseStore()
    {
        currentRoundNumber++;
        currentWaveNumber = 0;
        shopOwner.SetActive(false);
        PointerObject.SetActive(false);

        foreach(GameObject p in MultplayerController.instance.deadPlayers)
        {
            MultplayerController.instance.players.Add(p.GetComponent<Player>());
            PlayerStats _p = Instantiate(p, Vector2.zero, Quaternion.identity, null).GetComponent<PlayerStats>();
            _p.inv.gameObject.SetActive(true);
            foreach (Slot s in _p.inv.slots)
            {
                s.isPlayerDead = false;
            }
            MultplayerController.instance.deadPlayers.Remove(p);
        }

        NextWave();
    }

    public void OnEnemyDeath()
    {
        //Debug.Log("Morri");
        enemiesRemaningAlive--;

        if(enemiesRemaningAlive == 0)
        {
            NextWave();
        }
    }

    void OnBossDeath()
    {
        curBoss = null;
        NextWave();
    }

    void NextWave()
    {
        currentWaveNumber++;

        if(!waves[currentWaveNumber - 1].bossWave && !waves[currentWaveNumber - 1].shopWave)
        {
            if (currentWaveNumber - 1 < waves.Length)
            {
                currentWave = waves[currentWaveNumber - 1];
                ShowNextWaveText("Wave " + (currentWaveNumber).ToString());

                enemiesRemaningToSpawn = CalculateEnemiesToSpawn();
                enemiesRemaningAlive = enemiesRemaningToSpawn;
                maxEnemies = enemiesRemaningToSpawn;
            }
        }
        else if (waves[currentWaveNumber - 1].bossWave)
        {
            canSpawnBoss = true;
            bossTimer = timeToWaitUntilBoss;
            ShowNextWaveText("Boss Wave");
        }

        if (waves[currentWaveNumber - 1].shopWave)
        {
            ShowNextWaveText("Shop Wave");

            shopOwner.SetActive(true);
            PointerObject.SetActive(true);
        }
    }

    int CalculateEnemiesToSpawn()
    {  
        int enemiesToSpawn = Mathf.RoundToInt(
            ((MultplayerController.instance.players.Count*(
            currentWave.enemyCount + (currentRoundNumber * 2)
            - 1) / MultplayerController.instance.players.Count
            )) + 1.2f
        );
        Debug.Log("Enemies to spawn: " + enemiesToSpawn);
        return enemiesToSpawn;
    }

    public void SetTextFalse()
    {
        //nextWaveText.DOText("", .01f, false);
        //nextWaveText.gameObject.SetActive(false);
        nextWaveText.GetComponent<DOTweenAnimation>().DOPlayBackwards();
    }

    void ShowNextWaveText(string text)
    {
        nextWaveText.gameObject.SetActive(true);
        nextWaveText.text = text;
        //nextWaveText.GetComponent<DOTweenAnimation>();
        //nextWaveText.DOText(text, .75f, false);
        nextWaveTextAnim.DORestart();
        nextWaveTextAnim.DOPlay();
        nexWaveTextAnimator.SetTrigger("play");
    }

    [Serializable]
	public struct Wave
    {
        public string name;
        public int enemyCount;
        public float timeBetweenSpawns;
        public bool bossWave;
        public bool shopWave;
    }
}
