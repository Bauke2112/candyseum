﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EstatsScript : MonoBehaviour {

    public TextMeshProUGUI[] HighScores;
    public TextMeshProUGUI[] Bartext;

    private float WarriorTime;
    private float ArcherTime;
    private float MageTime;
    private float TankTime;
    private float proportionalDivisor;

    public Image[] timeBar;

    private void Awake()
    {
        StartCoroutine(Acao());
    }
    IEnumerator Acao()
    {
        

        TempoDeJogo();
        //ClasseMaisUltilizada();
        Grafico();

        if (PlayerPrefs.HasKey("HighRound") == true)                                                                                    // verifica de existe um HighRound
            HighScores[0].text = "Greater Round:" + PlayerPrefs.GetInt("HighRound").ToString();                                         // define oque estara escrito

        if (PlayerPrefs.HasKey("HighScore") == true)                                                                                    // verifica de existe um HighScore
            HighScores[1].text = "High Score:" + PlayerPrefs.GetInt("HighScore").ToString();                                            // define oque estara escrito

        if (PlayerPrefs.HasKey("EnemiesKilled") == true)                                                                                // verifica de existe um EnemiesKilled
            HighScores[2].text = "Killed Enemies:" + PlayerPrefs.GetInt("EnemiesKilled").ToString();                                    // define oque estara escrito

        if (PlayerPrefs.HasKey("BossKilled") == true)                                                                                    // verifica de existe um BossKilled
            HighScores[3].text = "Killed Bosses:" + PlayerPrefs.GetInt("BossKilled").ToString();                                         // define oque estara escrito


        // mostra todas as estatisticas no console
        Debug.Log(string.Format("Estasts: HighRound {0} , HighScore {1}, EnemiesKilled {2}, BossesKilled {3}, Tempo De Jogo {4} ClassesTime:{5} {6} {7} {8}", PlayerPrefs.GetInt("HighRound"), PlayerPrefs.GetInt("HighScore"), PlayerPrefs.GetInt("EnemiesKilled"), PlayerPrefs.GetInt("BossKilled"), PlayerPrefs.GetFloat("TimePlayed"), PlayerPrefs.GetFloat("WarriorTime"), PlayerPrefs.GetFloat("ArcherTime"), PlayerPrefs.GetFloat("MageTime"), PlayerPrefs.GetFloat("TankTime")));

        yield return new WaitForSeconds(15);

    }
    

    /*void ClasseMaisUltilizada()
    {
        WarriorTime = PlayerPrefs.GetFloat("WarriorTime");
        ArcherTime = PlayerPrefs.GetFloat("ArcherTime");
        MageTime = PlayerPrefs.GetFloat("MageTime");
        TankTime = PlayerPrefs.GetFloat("TankTime");


        if (WarriorTime > ArcherTime && WarriorTime > MageTime && WarriorTime > TankTime)
            if (WarriorTime / 60 < 1)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Warrior ({0}sec)", Mathf.RoundToInt(WarriorTime));

            else if (WarriorTime / 60 > 1 && WarriorTime / 60 < 59)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Warrior ({0}min)", Mathf.RoundToInt(PlayerPrefs.GetFloat("WarriorTime") / 60));

            else if (Mathf.RoundToInt(WarriorTime / 3600) > 1 && Mathf.RoundToInt(WarriorTime / 86400) < 24)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Warrior ({0}h)", Mathf.RoundToInt(PlayerPrefs.GetFloat("WarriorTime") / 3600));

            else
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Warrior ({0}d)", Mathf.RoundToInt(PlayerPrefs.GetFloat("WarriorTime") / 86400));

        if (ArcherTime > WarriorTime && ArcherTime > MageTime && ArcherTime > TankTime)
            if (ArcherTime / 60 < 1)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Archer ({0}sec)", Mathf.RoundToInt(ArcherTime));

            else if (ArcherTime / 60 > 1 && ArcherTime / 60 < 59)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Archer ({0}min)", Mathf.RoundToInt(PlayerPrefs.GetFloat("ArcherTime") / 60));

            else if (Mathf.RoundToInt(ArcherTime / 3600) > 1 && Mathf.RoundToInt(ArcherTime / 86400) < 24)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Archer ({0}h)", Mathf.RoundToInt(PlayerPrefs.GetFloat("ArcherTime") / 3600));

            else
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Archer ({0}d)", Mathf.RoundToInt(PlayerPrefs.GetFloat("ArcherTime") / 86400));

        if (MageTime > ArcherTime && MageTime > WarriorTime && MageTime > TankTime)

            if (MageTime / 60 < 1)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Mage ({0}sec)", Mathf.RoundToInt(MageTime));

            else if (MageTime / 60 > 1 && MageTime / 60 < 59)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Mage ({0}min)", Mathf.RoundToInt(PlayerPrefs.GetFloat("MageTime") / 60));

            else if (Mathf.RoundToInt(MageTime / 3600) > 1 && Mathf.RoundToInt(MageTime / 86400) < 24)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Mage ({0}h)", Mathf.RoundToInt(PlayerPrefs.GetFloat("MageTime") / 3600));

            else
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Mage ({0}d)", Mathf.RoundToInt(PlayerPrefs.GetFloat("MageTime") / 86400));

        if (TankTime > ArcherTime && TankTime > MageTime && TankTime > WarriorTime)
            if (TankTime / 60 < 1)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Tank ({0}sec)", Mathf.RoundToInt(TankTime));

            else if (TankTime / 60 > 1 && TankTime / 60 < 59)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Tank ({0}min)", Mathf.RoundToInt(PlayerPrefs.GetFloat("TankTime") / 60));

            else if (Mathf.RoundToInt(TankTime / 3600) > 1 && Mathf.RoundToInt(TankTime / 86400) < 24)
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Tank ({0}h)", Mathf.RoundToInt(PlayerPrefs.GetFloat("TankTime") / 3600));

            else
                HighScores[5].text = string.Format("Classe Mais Ultilizada: Tank ({0}d)", Mathf.RoundToInt(PlayerPrefs.GetFloat("TankTime") / 86400));
    }*/

    void TempoDeJogo()
    {
      if (PlayerPrefs.HasKey("TimePlayed") == true)                                                                                     // verifica de existe um TimePlayed
          if (PlayerPrefs.GetFloat("TimePlayed") / 60 < 1)                                                                              // verifica se foi jogado por mais de um minuto
              HighScores[4].text = string.Format("Game Time:{0}sec", Mathf.RoundToInt(PlayerPrefs.GetFloat("TimePlayed")));             // define oque estara escrito

          else if (PlayerPrefs.GetFloat("TimePlayed") / 60 > 1 && PlayerPrefs.GetFloat("TimePlayed") / 60 < 59)                         // verifica se foi jogado por mais de um minuto E menos de uma hora
              HighScores[4].text = string.Format("Game Time:{0}min", Mathf.RoundToInt(PlayerPrefs.GetFloat("TimePlayed") / 60));        // define oque estara escrito

          else if (PlayerPrefs.GetFloat("TimePlayed") / 3600 > 1 && PlayerPrefs.GetFloat("TimePlayed") / 86400 < 24)                    // verifica se foi jogado por mais de uma hora E menos de um dia
              HighScores[4].text = string.Format("Game Time:{0}h", Mathf.RoundToInt(PlayerPrefs.GetFloat("TimePlayed") / 3600));        // define oque estara escrito

          else
              HighScores[4].text = string.Format("Game Time:{0}d", Mathf.RoundToInt(PlayerPrefs.GetFloat("TimePlayed") / 86400));       // verifica se foi jogado por mais de um dia

    }

    void Grafico()
    {
        proportionalDivisor = Mathf.Max(PlayerPrefs.GetFloat("WarriorTime"), PlayerPrefs.GetFloat("ArcherTime"), PlayerPrefs.GetFloat("MageTime"), PlayerPrefs.GetFloat("TankTime"));   //Define o valor total a ser ultilizado na exibição no grafico
        timeBar[0].fillAmount = PlayerPrefs.GetFloat("WarriorTime") / proportionalDivisor;                                              // Define oa quantia a sere exibida 
        timeBar[1].fillAmount = PlayerPrefs.GetFloat("ArcherTime") / proportionalDivisor;                                               // Define oa quantia a sere exibida 
        timeBar[2].fillAmount = PlayerPrefs.GetFloat("MageTime") / proportionalDivisor;                                                 // Define oa quantia a sere exibida 
        timeBar[3].fillAmount = PlayerPrefs.GetFloat("TankTime") / proportionalDivisor;                                                 // Define oa quantia a sere exibida 

        if (PlayerPrefs.HasKey("WarriorTime") == true)                                                                                  // verifica de existe um TimePlayed
            if (PlayerPrefs.GetFloat("WarriorTime") / 60 < 1)                                                                           // verifica se foi jogado por mais de um minuto
                Bartext[0].text = string.Format("Warrior:{0}sec", Mathf.RoundToInt(PlayerPrefs.GetFloat("WarriorTime")));               // define oque estara escrito

            else if (PlayerPrefs.GetFloat("WarriorTime") / 60 > 1 && PlayerPrefs.GetFloat("WarriorTime") / 60 < 59)                     // verifica se foi jogado por mais de um minuto E menos de uma hora
                Bartext[0].text = string.Format("Warrior:{0}min", Mathf.RoundToInt(PlayerPrefs.GetFloat("WarriorTime") / 60));          // define oque estara escrito

            else if (PlayerPrefs.GetFloat("WarriorTime") / 3600 > 1 && PlayerPrefs.GetFloat("WarriorTime") / 86400 < 24)                // verifica se foi jogado por mais de uma hora E menos de um dia
                Bartext[0].text = string.Format("Warrior:{0}h", Mathf.RoundToInt(PlayerPrefs.GetFloat("WarriorTime") / 3600));          // define oque estara escrito

            else
                Bartext[0].text = string.Format("Warrior:{0}d", Mathf.RoundToInt(PlayerPrefs.GetFloat("WarriorTime") / 86400));         // verifica se foi jogado por mais de um dia
        else
            Bartext[0].text = "Warrior:-";

        if (PlayerPrefs.HasKey("ArcherTime") == true)                                                                                   // verifica de existe um TimePlayed
            if (PlayerPrefs.GetFloat("ArcherTime") / 60 < 1)                                                                            // verifica se foi jogado por mais de um minuto
                Bartext[1].text = string.Format("Archer:{0}sec", Mathf.RoundToInt(PlayerPrefs.GetFloat("ArcherTime")));                 // define oque estara escrito

            else if (PlayerPrefs.GetFloat("ArcherTime") / 60 > 1 && PlayerPrefs.GetFloat("ArcherTime") / 60 < 59)                       // verifica se foi jogado por mais de um minuto E menos de uma hora
                Bartext[1].text = string.Format("Archer:{0}min", Mathf.RoundToInt(PlayerPrefs.GetFloat("ArcherTime") / 60));            // define oque estara escrito

            else if (PlayerPrefs.GetFloat("ArcherTime") / 3600 > 1 && PlayerPrefs.GetFloat("ArcherTime") / 86400 < 24)                  // verifica se foi jogado por mais de uma hora E menos de um dia
                Bartext[1].text = string.Format("Archer:{0}h", Mathf.RoundToInt(PlayerPrefs.GetFloat("ArcherTime") / 3600));            // define oque estara escrito

            else
                Bartext[1].text = string.Format("Archer:{0}d", Mathf.RoundToInt(PlayerPrefs.GetFloat("ArcherTime") / 86400));           // verifica se foi jogado por mais de um dia
        else
            Bartext[1].text = "Archer:-";

        if (PlayerPrefs.HasKey("MageTime") == true)                                                                                     // verifica de existe um TimePlayed
            if (PlayerPrefs.GetFloat("MageTime") / 60 < 1)                                                                              // verifica se foi jogado por mais de um minuto
                Bartext[2].text = string.Format("Mage:{0}sec", Mathf.RoundToInt(PlayerPrefs.GetFloat("MageTime")));                     // define oque estara escrito

            else if (PlayerPrefs.GetFloat("MageTime") / 60 > 1 && PlayerPrefs.GetFloat("MageTime") / 60 < 59)                           // verifica se foi jogado por mais de um minuto E menos de uma hora
                Bartext[2].text = string.Format("Mage:{0}min", Mathf.RoundToInt(PlayerPrefs.GetFloat("MageTime") / 60));                // define oque estara escrito

            else if (PlayerPrefs.GetFloat("MageTime") / 3600 > 1 && PlayerPrefs.GetFloat("MageTime") / 86400 < 24)                      // verifica se foi jogado por mais de uma hora E menos de um dia
                Bartext[2].text = string.Format("Mage:{0}h", Mathf.RoundToInt(PlayerPrefs.GetFloat("MageTime") / 3600));                // define oque estara escrito

            else
                Bartext[2].text = string.Format("Mage:{0}d", Mathf.RoundToInt(PlayerPrefs.GetFloat("MageTime") / 86400));               // verifica se foi jogado por mais de um dia
        else
            Bartext[2].text = "Mage:-";

        if (PlayerPrefs.HasKey("TankTime") == true)                                                                                     // verifica de existe um TimePlayed
            if (PlayerPrefs.GetFloat("TankTime") / 60 < 1)                                                                              // verifica se foi jogado por mais de um minuto
                Bartext[3].text = string.Format("Tank:{0}sec", Mathf.RoundToInt(PlayerPrefs.GetFloat("TankTime")));                     // define oque estara escrito

            else if (PlayerPrefs.GetFloat("TankTime") / 60 > 1 && PlayerPrefs.GetFloat("TankTime") / 60 < 59)                           // verifica se foi jogado por mais de um minuto E menos de uma hora
                Bartext[3].text = string.Format("Tank:{0}min", Mathf.RoundToInt(PlayerPrefs.GetFloat("TankTime") / 60));                // define oque estara escrito

            else if (PlayerPrefs.GetFloat("TankTime") / 3600 > 1 && PlayerPrefs.GetFloat("TankTime") / 86400 < 24)                      // verifica se foi jogado por mais de uma hora E menos de um dia
                Bartext[3].text = string.Format("Tank:{0}h", Mathf.RoundToInt(PlayerPrefs.GetFloat("TankTime") / 3600));                // define oque estara escrito

            else
                Bartext[3].text = string.Format("Tank:{0}d", Mathf.RoundToInt(PlayerPrefs.GetFloat("TankTime") / 86400));               // verifica se foi jogado por mais de um dia
        else
            Bartext[3].text = "Tank:-";
    }
}
