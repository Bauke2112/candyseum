﻿using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Melee", order = 1)]
public class Melee : Weapon {

    public float attackSpeed;
    public float attackRange;
    public Animation attackAnim;
}
