﻿using UnityEngine;

public class Weapon : ScriptableObject {

    [Header("Transform properties")]
    [Range(0,5)]public float size;

    [Header("Helpers")]
    public bool isMelee;

    [Header("Weapon Data")]
    public Sprite weaponSprite;
    public new string name;
    public WeaponRarity rarity;

    [Header("Damage")]
    public float damage;
    public int criticChance = 5;
    public float criticDamage;
}

public enum WeaponRarity
{
    Common,
    Rare,
    Epic,
    Legendary,
    NoRarity
}
