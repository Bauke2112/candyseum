﻿using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Ranged", order = 1)]
public class Ranged : Weapon {

    public GameObject ammo;

    public float fireRate;

    public int maxAmmo;
    public int curAmmo;

    public float reloadTime;
}
