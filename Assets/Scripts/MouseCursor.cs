﻿using UnityEngine;

public class MouseCursor : MonoBehaviour {

    public int number;

    public float speed = 7f;

    private SpriteRenderer rend;

    private void Awake()
    {
        rend = this.GetComponent<SpriteRenderer>();

        if (rend == null)
            return;
    }

    private void Start()
    {
        Cursor.visible = false;
        transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y,0);

        if (number == 1)
            rend.color = Color.green;
        else if (number == 2)
            rend.color = Color.blue;
        else if (number == 3)
            rend.color = Color.yellow;
        else if (number == 4)
            rend.color = Color.red;
        else if (number == -10)
            rend.color = Color.gray;
    }

    private void Update()
    {
       /* if(number != -10)
        {
            transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);
            transform.Translate(new Vector2(InputManager.SecondHorizontal(number), InputManager.SecondVertical(number) * speed * Time.deltaTime));
        }else if(number == -10)
        {
            transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);
            transform.Translate(new Vector2(InputManager.SecondHorizontal(1), InputManager.SecondVertical(1) * speed * Time.deltaTime));
        }*/

        if(MultplayerController.instance.connectedControllers.Count != 0)
        {
            transform.Translate(new Vector3(InputManager.SecondHorizontal(number), -InputManager.SecondVertical(number), 0f) * Time.deltaTime * speed);
        }else if (MultplayerController.instance.connectedControllers.Count == 0)
        {
            transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0);
        }
    }
}
