﻿using UnityEngine;
using System.Collections;
using System;

namespace UnityLib {
    public static class UnityLib
    {
        public static void SetActive(this Component c, bool state)
        {
            c.gameObject.SetActive(state);
        }

        //Transform Extensions
        #region Transform Extensions
        //Set all transform properties to their default values
        public static void Reset(this Transform t)
        {
            t.position = Vector3.zero;
            t.rotation = Quaternion.Euler(Vector3.zero);
            t.localScale = Vector3.one;
        }

        //Change transform local scale
        public static void ChangeScale(this Transform t, Vector3 newScale)
        {
            t.localScale = newScale;
        }

        //Look at something (tranform, position, mouse)
        public static void LookAt2D(this Transform t, Vector3 targetPos)
        {
            var dir = (t.position - targetPos);
            dir.Normalize();
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
        }

        public static void LookAt2D(this Transform t, Transform target)
        {
            var dir = (t.position - target.position);
            dir.Normalize();
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
        }

        public static void LookAtMouse2D(this Transform t)
        {
            Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var dir = (t.position - targetPos);
            dir.Normalize();
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
        }
        #endregion

        //Rigidbody2D Extensions
        #region Rigidbody2D Extensions
        //Reset rigidbody velocity
        public static void ResetVelocity(this Rigidbody2D rb)
        {
            rb.velocity = Vector2.zero;
        }

        //Init 2D rigidbody correctly 
        public static void InitRigidbody2D(this Rigidbody2D rb, bool affectedByGravity, float mass)
        {
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            rb.mass = mass;

            if (affectedByGravity == false)
                rb.gravityScale = 0;
            else
                rb.gravityScale = 1;
        }

        //Change rigidbody gravity scale
        public static void SetGravityScale(this Rigidbody2D rb, float gravityScale)
        {
            rb.gravityScale = gravityScale;
        }

        //Get moving direction (position, transform, rigidbody)
        public static Vector3 GetDirection(this Rigidbody2D rb, Vector3 targetPos)
        {
            return (targetPos - rb.transform.position).normalized;
        }

        public static Vector3 GetDirection(this Rigidbody2D rb, Transform target)
        {
            return (target.position - rb.transform.position).normalized;
        }

        public static Vector3 GetDirection(this Rigidbody2D rb, Rigidbody2D target)
        {
            return (target.transform.position - rb.transform.position).normalized;
        }
        #endregion

        //Collider2D Extensions
        #region Collider2D Extensions

        #endregion

        //Sprite Renderer Extensions
        #region SpriteRenderer Extensions
        //Set sprite renderer new color
        public static void SetColor(this SpriteRenderer rend, Color color)
        {
            rend.color = color;
        }

        //Set sprite renderer new alpha without changing current r, g and b value
        public static void SetAlpha(this SpriteRenderer rend, float alpha)
        {
            Color c = rend.color;
            c.a = alpha;
            rend.color = c;
        }

        //Change alpha over time (Fade)
        public static IEnumerator FadeIn(this SpriteRenderer rend, float targetAlpha)
        {
            Color c = rend.color;

            while (rend.color.a != targetAlpha)
            {
                c.a += Time.deltaTime;
                rend.color = c;
                yield return 0;
            }
        }

        public static IEnumerator FadeOut(this SpriteRenderer rend, float targetAlpha)
        {
            Color c = rend.color;

            while (rend.color.a != targetAlpha)
            {
                c.a -= Time.deltaTime;
                rend.color = c;
                yield return 0;
            }
        }
        #endregion
    }
}