﻿using System.Collections;
using UnityEngine;

public class FreezeEffect : MonoBehaviour {

    public static FreezeEffect instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
    }

    [Range(0.0f, 1.5f)]
    public float duration;

    bool isFrozen = false;
    float pendingFrozenDuration = 0f;

    private void Update()
    {
        if(pendingFrozenDuration > 0 && !isFrozen)
        {
            StartCoroutine(DoFreeze());
        }
    }

    public void Freeze()
    {
        pendingFrozenDuration = duration;
    }

    IEnumerator DoFreeze()
    {
        isFrozen = true;
        var original = Time.timeScale;
        Time.timeScale = 0;

        yield return new WaitForSecondsRealtime(duration);

        Time.timeScale = original;
        pendingFrozenDuration = 0f;
        isFrozen = false;
    }
}
