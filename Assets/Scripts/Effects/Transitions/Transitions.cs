﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Transitions : MonoBehaviour {

    public static Transitions instance;
    public Animator fadeAnim;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
    }

    public IEnumerator LoadScene(string sceneName)
    {
        if(AudioController.audioController != null)
            AudioTransitions.AudioTransition(AudioController.audioController.gameObject.GetComponent<Animator>());

        fadeAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.1f);
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public IEnumerator LoadScene(int sceneId)
    {
        if (AudioController.audioController != null)
            AudioTransitions.AudioTransition(AudioController.audioController.gameObject.GetComponent<Animator>());

        fadeAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.1f);
        SceneManager.LoadScene(sceneId, LoadSceneMode.Single);
    }
}
