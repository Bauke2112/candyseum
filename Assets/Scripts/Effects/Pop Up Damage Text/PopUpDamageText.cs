﻿using TMPro;
using UnityEngine;

public class PopUpDamageText : MonoBehaviour {

    public Vector3 offset = new Vector2(0f, 2f);
    public float destroyTime = 3f;

    public Color[] colors;
    private int index;

    TextMeshProUGUI text;

    private void Awake()
    {
        text = this.GetComponent<TextMeshProUGUI>();

        if (text == null)
            return;
    }

    private void Start()
    {
        index = Random.Range(0, colors.Length);
        text.color = colors[index];

        transform.position += offset;
        Destroy(this.gameObject, destroyTime);
    }

    public void SetText(string newText)
    {
        text.text = newText;
    }

    public void SetText(float damage)
    {
        text.text = damage.ToString();
    }
}
