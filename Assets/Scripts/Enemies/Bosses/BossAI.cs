﻿using UnityEngine;
using UnityEngine.UI;

public class BossAI : MonoBehaviour
{
    #region Variables
    public event System.Action OnDeath; //evento de morte do boss
    private EnemyDrop dropTable; //Drops do Boss

    public float hp { get; set; }

    public float timeToWait = 60f;
    private float timer;

    public bool canStart = false;
    public int bossStage = 1; 
    // 1 => primeiro estágio, 2 => segundo estágio, 3 => terceiro estágio
    // 1 => 100 - 70% hp
    // 2 => 70 - 30% hp
    // 3 => 30 - 0% hp

    public Rigidbody2D rb;
    public Animator anim;

    [Header("Movement Variables")]
    private Vector2 direction; //direção na qual o inimigo se move
    public float rayDistance = 2f; //distância para checar obstáculos
    public float dodgeDistance = .5f; //distância para desviar de obstáculos
    public LayerMask obstaclesLayer; //LayerMAsk dos obstáculos
    public int animVelocity;

    [Header("Boss Specific Variables")]
    public Boss bossAIFile;

    public Transform target; //transform do player
    public bool canMove;

    public Image healthBar;
    #endregion

    private void Awake() //configurando váriaveis
    {
        rb = GetComponent<Rigidbody2D>();
        dropTable = this.gameObject.GetComponent<EnemyDrop>();
        anim = this.GetComponent<Animator>();
        //canMove = true;

        if (dropTable == null)
            return;

        hp = bossAIFile.maxHp;
        timer = timeToWait;
    }

    public virtual void Start()
    {
        target = ChooseTarget();
        healthBar = GameObject.FindGameObjectWithTag("BossLifeBar").GetComponent<Image>();
    }

    public virtual void Update()
    {
        if(timer <= 0)
        {
            canStart = true;
            canMove = true;
        }else
        {
            timer -= Time.deltaTime;
        }

        if (canMove)
        {
            Move();
            AvoidObstacles();
        }

        if (GetFillAmount() < .7f && GetFillAmount() > .3f)
            bossStage = 2;
        else if (GetFillAmount() < .3f)
            bossStage = 3;

        if(healthBar != null)
            healthBar.fillAmount = GetFillAmount();
    }

    private void Move()
    {
        if (target == null)
            return;

        float distance = Vector2.Distance(this.transform.position, target.position);

        if(distance > bossAIFile.stopDistance)
        {
            direction = (target.transform.position - this.transform.position).normalized;
            rb.velocity = (direction * bossAIFile.speed);
            animVelocity = 1;
        }
        else if(distance <= bossAIFile.stopDistance)
        {
            animVelocity = 0;
            return;
        }

        //rbBoss.velocity = Vector2.MoveTowards(this.transform.position, target.position, bossAIFile.speed * Time.deltaTime);
    }

    Transform ChooseTarget()
    {
        int randIndex = Random.Range(0, MultplayerController.instance.players.Count);
        return MultplayerController.instance.players[randIndex].transform;
    }

    public float GetFillAmount() //retorna quantidade de vida do boss em um número entre 0 e 1
    {
        float t = hp / bossAIFile.maxHp;

        return t;
    }

    public void Damage(float damage)
    {
        hp -= damage;

        Die();
    }

    private void Die()
    {
        if (hp <= 0)
        {
            healthBar.fillAmount = 0f;

            if (PlayerPrefs.HasKey("BossKilled") == true) 
                PlayerPrefs.SetInt("BossKilled", PlayerPrefs.GetInt("BossKilled") + 1);
            else
                PlayerPrefs.SetInt("BossKilled", 1);

            EnemySpawnAI.addScore = Random.Range(30, 80);
            if (OnDeath != null)
                OnDeath();

            if (dropTable != null)
            {
                dropTable.DropItem();
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }

            PlayerGold.instance.goldQuant += Random.Range(bossAIFile.minGoldQuant, bossAIFile.maxGoldQuant);
        }
    }

    void AvoidObstacles() //função que faz o inimigo desviar dos obstáculos do mapa
    {
        RaycastHit2D hit = new RaycastHit2D();

        hit = Physics2D.Raycast(this.transform.position, direction, rayDistance, obstaclesLayer);

        if (hit.collider == null)
            return;
        else
        {
            if (Vector2.Distance(this.transform.position, hit.transform.position) >= dodgeDistance)
            {
                //Avoid obstacle
                direction = new Vector2(hit.transform.position.x + 6f, hit.transform.position.y + 6f);
            }
        }
    }
}

public enum BossType
{
    //Fazer tipos de bosses
    Cookie,
    Marshmallow,
    Lollipop,
    Brigadeiro,
    Brownie,
    Bear,
    Candy_Machine,
    Waffle
}
