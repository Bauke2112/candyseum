﻿using UnityEngine;

[CreateAssetMenu(fileName = "Boss", menuName = "Boss/Boss AI File", order = 1)]
public class Boss : ScriptableObject {

    public new string name;
    public float speed;

    public float stopDistance;
    public BossType bossType;

    public float maxHp;
    public int minGoldQuant;
    public int maxGoldQuant;
}
