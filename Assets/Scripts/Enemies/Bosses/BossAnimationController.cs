﻿using UnityEngine;

public class BossAnimationController : MonoBehaviour {

    private BossAI myAI;
    private Animator anim;

    private void Awake()
    {
        myAI = this.GetComponent<BossAI>();
        anim = this.GetComponent<Animator>();

        if (myAI == null || anim == null)
            return;
    }

    private void Update()
    {
        anim.SetInteger("velocity", myAI.animVelocity);
    }
}
