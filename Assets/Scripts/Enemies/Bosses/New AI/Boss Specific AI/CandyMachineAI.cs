﻿using UnityEngine;
using System.Collections;

public class CandyMachineAI : BossAI {

    [Header("Stage One")]
    public GameObject stageOneBullet;
    public float fireRate = .6f;

    [Header("Stage Two")]
    public GameObject stageTwoBullet;
    public float secondFireRate = .4f;

    [Header("Stage Three")]
    public GameObject stageThreeBullet;
    public float thirdFireRate;

    public bool stageOne;
    public bool stageTwo;
    public bool stageThree;

    private GameObject stageTwoObject;

    public override void Start()
    {
        base.Start();
        //InvokeRepeating("Shoot",1.2f,fireRate);
    }

    public override void Update()
    {
        base.Update();

        if (!this.canStart)
            return;

        if(this.bossStage == 1 && !stageOne)
        {
            stageOne = true;
            InvokeRepeating("Shoot", 0f, fireRate);
        }

        if(this.bossStage == 2 && !stageTwo)
        {
            rb.velocity = Vector2.zero;
            stageTwo = true;
            StageTwo();
        }

        if(this.bossStage == 3 && !stageThree)
        {
            CancelInvoke();
            this.anim.SetBool("stageTwo", false);
            this.stageThree = true;
            this.canMove = true;

            StartCoroutine(StageThreeShoot());
        }
    }

    public void StageTwo()
    {
        this.anim.SetBool("stageTwo", true);
        this.canMove = false;

        InvokeRepeating("StageTwoShoot", 0f, secondFireRate);
    }

    public void StageTwoShoot()
    {
        Instantiate(stageTwoBullet, this.transform.position, Quaternion.identity, null);
    }

    IEnumerator StageThreeShoot()
    {
        CancelInvoke();
        Instantiate(stageThreeBullet, this.transform.position, Quaternion.identity);
        InvokeRepeating("Shoot", 0.2f, fireRate);
        yield return new WaitForSeconds(thirdFireRate);
    }

    public void Shoot()
    {
        if (target == null)
            return;

        BossBullet b = Instantiate(stageOneBullet, this.transform.position, Quaternion.identity, null).GetComponent<BossBullet>();
        b.dir = (this.target.position - this.transform.position).normalized;
    }
}
