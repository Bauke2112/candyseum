﻿using UnityEngine;

public class CookieAI : BossAI {

    [Header("Stage One")]
    public GameObject stageOneBullet;
    public float fireRate;
    public bool stageOne;

    [Header("Stage Two")]
    public GameObject stageTwoBullet;

    public bool stageTwo;
    private GameObject stageTwoObject;

    [Header("Stage Three")]
    public bool stageThree;
    public float spawnRate;
    public GameObject spawnObject;

    public override void Start()
    {
        base.Start();
        this.canMove = false;
        //InvokeRepeating("Shoot", 1.2f, fireRate);
    }

    public override void Update()
    {
        base.Update();

        if (!this.canStart)
            return;

        if (this.bossStage == 1 && !stageOne)
        {
            stageOne = true;
            InvokeRepeating("Shoot", 0f, fireRate);
        }

        if(this.bossStage == 2 && !stageTwo)
        {
            stageTwo = true;
            CancelInvoke();
            Invoke("StageTwoShoot", 0.4f);
        }

        if (this.bossStage == 3 && !stageThree)
        {
            stageThree = true;
            CancelInvoke();
            Destroy(stageTwoObject);
            InvokeRepeating("Shoot", 0f, fireRate);
            InvokeRepeating("Chocolate", 0f, spawnRate);
        }
    }

    public void StageTwoShoot()
    {
        stageTwoObject = Instantiate(stageTwoBullet, this.transform.position, Quaternion.identity);
        stageTwoObject.transform.localPosition = Vector3.zero;
    }

    public void Shoot()
    {
        Vector3 rt = new Vector3(0f, 0f, Random.Range(0f, 360f));
        Quaternion real_rt = Quaternion.Euler(rt);

        BossBullet b = Instantiate(stageOneBullet, this.transform.position, real_rt, null).GetComponent<BossBullet>();

        if (target == null)
            return;

        b.dir = (this.target.position - this.transform.position).normalized;
    }

    public void Chocolate()
    {
        Vector3 rt = new Vector3(0f, 0f, 0f);
        Quaternion real_rt = Quaternion.Euler(rt);

        BossBullet b = Instantiate(spawnObject, this.transform.position, real_rt, null).GetComponent<BossBullet>();

        if (target == null)
            return;

        b.dir = (this.target.position - this.transform.position).normalized;
    }
}
