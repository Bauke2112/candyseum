﻿using UnityEngine;

public class CoinBullet : MonoBehaviour {

    public float timeToMove = 1.2f;
    public float speed;
    public float timeToChange;
    public float timeToDestroy;
    public float damage = 20f;

    private Rigidbody2D rb;
    private float timer;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();

        timer = timeToChange;
        Destroy(this.gameObject, timeToDestroy);

        Invoke("SetVelocity", timeToMove);
    }

    void SetVelocity()
    {
        rb.velocity = transform.right * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            collision.GetComponent<PlayerStats>().Damage(damage);
            Destroy(this.gameObject);
        }
    }
}
