﻿using UnityEngine;

public class CandyMachineStageTwo : MonoBehaviour {

    public float timeToDestroy;
    public Transform[] positionsToSpawn;
    [Space]
    public GameObject bullet;
    public float fireRate;

    private void Start()
    {
        Destroy(this.gameObject, timeToDestroy);
        InvokeRepeating("Shoot", .5f, fireRate);
    }

    void Shoot()
    {
        foreach(Transform t in positionsToSpawn)
        {
            Instantiate(bullet, t.position, Quaternion.identity, t);
        }
    }
}
