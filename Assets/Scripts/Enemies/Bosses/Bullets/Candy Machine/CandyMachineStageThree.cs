﻿using UnityEngine;

public class CandyMachineStageThree : MonoBehaviour {

    public float xRadius;
    public float yRadius;

    public int bombsToSpawn;

    public GameObject shadowBombEffect;

    private void Start()
    {
        SpawnBombs();
    }

    private void SpawnBombs()
    {
        for(int i = 0; i < bombsToSpawn; i++)
        {
            float x, y;
            x = Random.Range(-xRadius, xRadius);
            y = Random.Range(-yRadius, yRadius);
            Vector2 pos = new Vector2(x, y);

            Instantiate(shadowBombEffect, pos, Quaternion.identity);
        }
    }
}
