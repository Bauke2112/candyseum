﻿using UnityEngine;

public class ShadowEffectBullet : MonoBehaviour {

    public float explosionRadius;
    public float timeToExplode;
    public GameObject explosionEffect;

    public float damage;
    private float timer;

    private void Awake()
    {
        timer = timeToExplode;
    }

    private void Update()
    {
        if(timer <= 0)
        {
            Collider2D[] hits = Physics2D.OverlapCircleAll(this.transform.position, explosionRadius);

            foreach(Collider2D c in  hits)
            {
                PlayerStats stats = c.GetComponent<PlayerStats>();

                if (stats == null)
                    return;

                stats.Damage(damage);
            }

            Instantiate(explosionEffect, this.transform.position, Quaternion.identity, null);
            Destroy(gameObject);
        } else
        {
            timer -= Time.deltaTime;
        }
    }
}
