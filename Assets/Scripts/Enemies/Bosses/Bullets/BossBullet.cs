﻿using UnityEngine;

public class BossBullet : MonoBehaviour {

    public float speed = 14f;
    public float damage = 10f;
    public float timeToDestroy = 5f;
    public float magnitude = .014f;
    public float frequency = .14f;

    [HideInInspector] public Vector2 dir;
    private Rigidbody2D rb;

    public bool sinWave;
    public bool shootStraight;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();

        if (rb == null)
            return;
    }

    private void Start()
    {
        Destroy(this.gameObject, timeToDestroy);
    }

    private void Update()
    {
        if (!sinWave && !shootStraight)
            rb.velocity = dir * speed;
        else if (sinWave && !shootStraight)
        {
            Vector3 pos = transform.position + transform.right * speed * Time.deltaTime;
            rb.MovePosition(pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude);
        }
        else if(!sinWave && shootStraight)
        {
            rb.velocity = transform.right * speed;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerStats>().Damage(damage);
            KnockBack(-6000f, other.GetComponent<Rigidbody2D>(), other.transform);
            Destroy(this.gameObject);
        }
    }

    public void KnockBack(float knockbackPower, Rigidbody2D otherRb, Transform otherTrans)
    {
        var dir = (transform.position - otherTrans.position).normalized;

        otherRb.AddForce(dir * knockbackPower);
    }
}
