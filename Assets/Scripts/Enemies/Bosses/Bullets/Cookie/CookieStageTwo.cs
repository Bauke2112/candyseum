﻿using UnityEngine;

public class CookieStageTwo : MonoBehaviour {

    public GameObject bullet;
    public float fireRate;

    public float rotSpeed = 7;

    public Transform pos1;
    public Transform pos2;
    public Transform pos3;

    private void Start()
    {
        InvokeRepeating("Shoot", 0f, fireRate);
    }

    private void Update()
    {
        this.transform.Rotate(new Vector3(0f, 0f, rotSpeed));
    }

    void Shoot()
    {
        Instantiate(bullet, pos1.position, Quaternion.Euler(new Vector3(0f, 0f, 90f)), this.transform);
        Instantiate(bullet, pos2.position, Quaternion.Euler(new Vector3(0f, 0f, 225f)), this.transform);
        Instantiate(bullet, pos3.position, Quaternion.Euler(new Vector3(0f, 0f, 315f)), this.transform);
    }
}
