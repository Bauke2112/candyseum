﻿using UnityEngine;

public class CookieStageTwoBullet : MonoBehaviour {

    public float speed = 14;
    public float damage = 7.5f;
    public float timeToDestroy = 20f;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, timeToDestroy);
    }

    private void Update()
    {
        rb.velocity = transform.right * speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerStats>().Damage(damage);
            Destroy(this.gameObject);
        }
    }
}
