﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EnemyController : MonoBehaviour
{
    #region Variables
    public float stopDistance = 10f;

    public enum Type { Lollipop, Marshmallow, Cookie, Brownie, Brigadeiro, Chocolate }; //tipo do inimigo
    public Type EnemyType; //tipo do inimigo

    public float speed = 3f; //velocidade

    public bool isChasing;

    public List<GameObject> players = new List<GameObject>();

    public Transform targetToFollow;

    [HideInInspector] public int fixedSpeed; //usado para animações

    private Rigidbody2D rb;

    public GameObject bullet;
    public float fireRate = 1f;

    private float timer;
    private bool canShoot = true;

    private Vector2 direction; //direção na qual o inimigo se move
    public float rayDistance = 2f; //distância para checar obstáculos
    public float dodgeDistance = .5f; //distância para desviar de obstáculos
    public LayerMask obstaclesLayer; //LayerMAsk dos obstáculos

    public float healAmount = 2f; //apenas para inimigo tipo brownie

    public GameObject ExplosiondeathEffect; //efeito de explosao para o brigadeiro

    private EnemySpawnAI enemyStats;
    #endregion

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        enemyStats = this.GetComponent<EnemySpawnAI>();

        if (rb == null || enemyStats == null)
            return;
    }

    void Start() {

        foreach (Player p in MultplayerController.instance.players)
        {
            players.Add(p.gameObject);
        }

        ChoosePlayerToFollow();
    }

    void Update() {

        if (EnemyType != Type.Brigadeiro)
        {
            if (canShoot)
                Shoot();
        }


        ResetCanShoot();

        if (this.EnemyType != Type.Brownie)
        {
            if (MultplayerController.instance.players.Count == 0)
                return;

            NoTargetToFollow();
            Follow(speed);
        } else if (this.EnemyType == Type.Brownie)
        {
            targetToFollow = ChooseEnemyToDefend();

            DefendEnemy(speed);

            if (canShoot)
                HealEnemy();
        }

        AvoidObstacles();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(this.transform.position, direction * rayDistance);
    }

    void ChoosePlayerToFollow() {
        int index = Random.Range(0, players.Count);
        targetToFollow = players[index].transform;
        //Debug.Log(PlayerToFollow.name);
    }

    void NoTargetToFollow() {
        int index = Random.Range(0, players.Count);
        if (targetToFollow == null || targetToFollow.gameObject.activeSelf == false) {
            targetToFollow = players[index].transform;
        } else {
            return;
        }

        fixedSpeed = 0;
    }

    void Follow(float speed) {
        if (targetToFollow == null)
            return;

        if (Vector2.Distance(this.transform.position, targetToFollow.position) > stopDistance)
        {
            direction = (targetToFollow.position - this.transform.position).normalized; //calcula direção do movimento
            rb.velocity = direction * speed;

            fixedSpeed = (int)Mathf.Clamp(rb.velocity.y, -1f, 1f);
        } else if (Vector2.Distance(this.transform.position, targetToFollow.position) <= stopDistance)
        {
            direction = Vector2.zero;
            rb.velocity = direction;

            fixedSpeed = (int)Mathf.Clamp(rb.velocity.y, -1f, 1f);
        }


        if ((EnemyType == Type.Brigadeiro || EnemyType == Type.Chocolate) && Vector2.Distance(this.transform.position, targetToFollow.position) <= stopDistance)
        {          

            Spawner.instance.enemiesRemaningAlive -= 1;
            GetComponent<EnemyController>().enabled = false;


            if (EnemyType == Type.Brigadeiro)
            {
                targetToFollow.GetComponent<PlayerStats>().Damage(25);
                Instantiate(ExplosiondeathEffect, this.transform.position, this.transform.rotation);
            }
            else if (EnemyType == Type.Chocolate)
                targetToFollow.GetComponent<PlayerStats>().reduceSpeed(3, 4, 4);

            Destroy(gameObject);
        }
    }

    void Shoot()
    {
        canShoot = false;
        timer = fireRate;

        if (targetToFollow == null)
            return;

        Vector2 dir = (targetToFollow.position - this.transform.position).normalized;

        EnemyBullet eb = Instantiate(bullet, transform.position, Quaternion.identity, null).GetComponent<EnemyBullet>();
        eb.damage += this.enemyStats.damage;
        eb.dir = dir;
    }

    void ResetCanShoot()
    {
        if (!canShoot && timer > 0)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                canShoot = true;
                timer = 0;
            }
        }
    }

    void AvoidObstacles() //função que faz o inimigo desviar dos obstáculos do mapa
    {
        RaycastHit2D hit = new RaycastHit2D();

        hit = Physics2D.Raycast(this.transform.position, direction, rayDistance, obstaclesLayer);

        if (hit.collider == null)
            return;
        else
        {
            if (Vector2.Distance(this.transform.position,hit.transform.position) >= dodgeDistance)
            {
                //Avoid obstacle
                direction = new Vector2(hit.transform.position.x + 6f, hit.transform.position.y + 6f);
            }
        }
    }
    
    //Funções específicas de algum tipo de inimigo
    #region Brownie Functions 
    Transform ChooseEnemyToDefend()
    {
        List<GameObject> enemies = GameObject.FindGameObjectsWithTag("Enemy").ToList<GameObject>();
        float dist;
        float minDist = float.MaxValue;

        if(enemies.Count > 1)
        {
            foreach (GameObject _e in enemies)
            {
                EnemyController controller = _e.GetComponent<EnemyController>();

                if (controller.EnemyType == this.EnemyType)
                {
                    enemies.Remove(controller.gameObject);
                }

                dist = Vector2.Distance(this.transform.position, _e.transform.position);

                if (dist < minDist)
                {
                    minDist = dist;
                    return _e.transform;
                }
            }
        }else if(enemies.Count == 1)
        {
            int index = Random.Range(0, players.Count);
            return players[index].transform;
        }

        return null;
    }

    void DefendEnemy(float speed)
    {
        if (Vector2.Distance(this.transform.position, targetToFollow.position) > stopDistance)
        {
            direction = (targetToFollow.transform.position - this.transform.position + new Vector3(0f,-1f,0f)).normalized;
            rb.velocity = direction * speed;

            fixedSpeed = (int)Mathf.Clamp(rb.velocity.y, -1f, 1f);
        }else if (Vector2.Distance(this.transform.position, targetToFollow.position) <= stopDistance)
        {
            direction = Vector2.zero;
            rb.velocity = direction;

            fixedSpeed = (int)Mathf.Clamp(rb.velocity.y, -1f, 1f);
        }
    }

    void HealEnemy()
    {
        canShoot = false;
        timer = fireRate;

        EnemySpawnAI _e = targetToFollow.GetComponent<EnemySpawnAI>();

        if (_e == null)
            return;

        if (_e.hp + healAmount > _e.maxHp)
            _e.hp += 0;
        else
            _e.hp += healAmount;
    }
    #endregion //Funções do inimigo tipo Brownie

}
