﻿using UnityEngine;

public class EnemyAnimationController : MonoBehaviour {

    private EnemyController controller;
    private Animator animController;

    private void Awake()
    {
        controller = GetComponent<EnemyController>();
        animController = GetComponent<Animator>();

        if (controller == null || animController == null)
            return;
    }

    private void Update()
    {
        animController.SetInteger("velocity", controller.fixedSpeed);
    }
}
