﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyBullet : MonoBehaviour {

    public Vector2 dir;
    public float speed = 10f;

    public float damage = 5;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();

        if (rb == null)
            return;

        Destroy(gameObject, 2f);
    }

    private void FixedUpdate()
    {
        rb.velocity = dir * speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerStats>().Damage(damage);
            KnockBack(-2500f, other.GetComponent<Rigidbody2D>(), other.transform);
            Destroy(this.gameObject);
        }
    }

    public void KnockBack(float knockbackPower, Rigidbody2D otherRb, Transform otherTrans)
    {
        var dir = (transform.position - otherTrans.position).normalized;

        otherRb.AddForce(dir * knockbackPower);
    }
}
