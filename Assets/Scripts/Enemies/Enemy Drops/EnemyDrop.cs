﻿using UnityEngine;
using System.Collections.Generic;

public class EnemyDrop : MonoBehaviour {

    public GameObject item;
    public GameObject weaponItem;
    public List<DropTable> drops = new List<DropTable>();
    public List<WeaponDropTable> weaponDrops = new List<WeaponDropTable>();

    public void DropItem()
    {
        int roll = UnityEngine.Random.Range(0, 101);
        int rateSum = 0;
        foreach(DropTable drop in drops)
        {
            rateSum += drop.dropRate;
            if(roll < rateSum)
            {
                Vector2 pos = new Vector2(transform.position.x + Random.Range(-1.7f,1.7f), transform.position.y + Random.Range(-1.7f, 1.7f));
                Item _i = Instantiate(item, pos, Quaternion.identity, null).GetComponent<Item>();

                _i.item = drop.itemToDrop;
                return;
            }
        }
    }

    public void DropWeapon()
    {
        int roll = UnityEngine.Random.Range(0, 101);
        float rateSum = 0;
        int randomIndex = 0;

        List<WeaponDropTable> possibleDrops = new List<WeaponDropTable>();

        foreach(WeaponDropTable wd in weaponDrops)
        {
            if (wd.weaponToDrop.rarity == WeaponRarity.Common)
                wd.actualDropRate = DropTableManager.commonDropChance * wd.dropRate;
            else if (wd.weaponToDrop.rarity == WeaponRarity.Rare)
                wd.actualDropRate = DropTableManager.rareDropChance * wd.dropRate;
            else if (wd.weaponToDrop.rarity == WeaponRarity.Rare)
                wd.actualDropRate = DropTableManager.epicDropChance * wd.dropRate;
            else if (wd.weaponToDrop.rarity == WeaponRarity.Rare)
                wd.actualDropRate = DropTableManager.legDropChance * wd.dropRate;
            else if (wd.weaponToDrop.rarity == WeaponRarity.NoRarity)
                wd.actualDropRate = wd.dropRate;

            var actualChance = wd.actualDropRate;

            rateSum += actualChance;
            if(roll < rateSum)
            {
                possibleDrops.Add(wd);
                randomIndex = Random.Range(0, possibleDrops.Count);
            }
        }

        Debug.Log("Weapon to drop: " + possibleDrops[randomIndex].weaponToDrop.name);
        Vector2 pos = new Vector2(transform.position.x + Random.Range(-1.7f, 1.7f), transform.position.y + Random.Range(-1.7f, 1.7f));
        WeaponItem _i = Instantiate(weaponItem, pos, Quaternion.identity, null).GetComponent<WeaponItem>();

        _i.item = possibleDrops[randomIndex].weaponToDrop;
        return;
    }
}
