﻿using UnityEngine;

public static class DropTableManager
{
    public static float commonDropChance = .6f;
    public static float rareDropChance = .45f;
    public static float epicDropChance = .2f;
    public static float legDropChance = .01f;
}