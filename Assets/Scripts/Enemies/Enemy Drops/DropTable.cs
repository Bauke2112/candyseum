﻿using UnityEngine;

[System.Serializable]
public class DropTable {

    public string name = "New Drop";
    public Things itemToDrop; //item que será dropado
    public int dropRate; //porcentagem de drop de cada item
}

[System.Serializable]
public class WeaponDropTable {

    public string name = "New Weapon Drop";
    public Weapon weaponToDrop;
    public int dropRate;

    public float actualDropRate { get; set; }
}
