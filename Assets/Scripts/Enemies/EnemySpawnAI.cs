﻿using UnityEngine;

public class EnemySpawnAI : MonoBehaviour {

    public event System.Action OnDeath;

    private EnemyDrop dropTable;
    public float manaBonus = 5f;

    public static int addScore = 0;

    public bool forceDie;

    public float maxHp;
    //[HideInInspector]
    public float hp;

    public int maxGoldAmount = 35;
    public int minGoldQuant = 5;

    public GameObject deathEffect;
    public static GameObject WalldeathEffect;
    public GameObject bloodEffect;

    private AudioSource source;
    public int damage { get; private set; }

    public float GetFillAmount()
    {
        float t = hp / maxHp;
        //Debug.LogWarning(t);
        return t;
    }

    private void Awake()
    {
        WalldeathEffect = deathEffect;
        dropTable = this.gameObject.GetComponent<EnemyDrop>();
        source = this.GetComponent<AudioSource>();

        if (dropTable == null || source == null)
            return;

        hp = maxHp;
        ChangeStats();
    }

    public void Damage(float damage)
    {
        hp -= damage;

        Die();
    }

    private void Die()
    {
        if (hp <= 0)
        {
            if(forceDie == true)
            {                
                Instantiate(deathEffect, this.transform.position, this.transform.rotation);
                Instantiate(bloodEffect, this.transform.position, Quaternion.identity, null);
                Destroy(this.gameObject);
                return;
            }
                

            addScore = Random.Range(1, 10);
            
            if (PlayerPrefs.HasKey("EnemiesKilled") == true)
            {                
                PlayerPrefs.SetInt("EnemiesKilled", PlayerPrefs.GetInt("EnemiesKilled") + 1);     
            }
            else
            {
                PlayerPrefs.SetInt("EnemiesKilled", 1);
            }
            if (OnDeath != null)
                OnDeath();

            Instantiate(deathEffect, this.transform.position, this.transform.rotation);
            source.Play();
            Instantiate(bloodEffect, this.transform.position, Quaternion.identity, null);

            foreach (Player p in MultplayerController.instance.players)
            {
                PlayerStats stats = p.GetComponent<PlayerStats>();
                if((stats.mana += manaBonus) > stats.maxMana)
                {
                    stats.mana += (stats.maxMana - stats.mana);
                }else
                {
                    stats.mana += manaBonus;
                }
            }

            if (dropTable != null)
            {
                dropTable.DropItem();
                dropTable.DropWeapon();
                Destroy(this.gameObject);
            }
            else
            {
               
                FreezeEffect.instance.Freeze();
                Destroy(this.gameObject);
            }

            PlayerGold.instance.goldQuant += Random.Range(minGoldQuant, maxGoldAmount);
           
        }

    }

    private void ChangeStats()
    {
        //Change enemy stats over time
        int _hp, _damage;
        _hp = Mathf.RoundToInt(Spawner.instance.currentRoundNumber + .1f * Spawner.instance.currentWaveNumber);
        _damage = Mathf.RoundToInt(Spawner.instance.currentRoundNumber + .1f * Spawner.instance.currentWaveNumber);

        maxHp += _hp;
        hp = maxHp;

        Debug.Log("===========================");
        Debug.Log("Enemy stats updated!");
        Debug.Log("===========================");
    }
}
