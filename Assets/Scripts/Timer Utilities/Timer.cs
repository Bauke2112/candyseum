﻿using UnityEngine;
using System.Collections.Generic;

namespace TimerLib
{
    public static class Timer
    {
        public static List<TimerObject> timers = new List<TimerObject>();

        public static TimerObject CreateTimer(float startTime, bool decrease)
        {
            GameObject go = new GameObject("Timer");
            TimerObject timer = go.AddComponent<TimerObject>();
            timer.SetTimer(startTime, decrease);
            timers.Add(timer);
            return timer;
        }

        public static void ClearTimers()
        {
            timers.Clear();
        }
    }
}
