﻿using UnityEngine;
using System;

public class TimerObject : MonoBehaviour
{
    public float startTime = 5f;
    public bool decrease = true;

    public float timer { get; private set; }
    private bool updating = false;

    public delegate void TimerEventHandler(object source, EventArgs args);
    public event TimerEventHandler TimerFinished;

    public void SetTimer(float t, bool d)
    {
        startTime = t;
        decrease = d;

        if (decrease)
            timer = startTime;
        else
            timer = 0;
    }

    public void StartTimer()
    {
        updating = true;
    }

    public void StopTimer()
    {
        updating = false;
        Destroy(this.gameObject);
    }

    public float NormalizedTime()
    {
        return timer / startTime;
    }

    public string StringTimer()
    {
        return timer.ToString();
    }

    protected virtual void OnTimerFinished()
    {
        if (TimerFinished != null)
            TimerFinished(this, EventArgs.Empty);
    }

    private void Update()
    {
        if (updating == false)
            return;

        if (decrease)
        {
            if (timer <= 0)
            {
                //Do something
                OnTimerFinished();
                Debug.Log("Time out");
                this.StopTimer();
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
        else
        {
            if (timer >= startTime)
            {
                //Do something
                OnTimerFinished();
                Debug.Log("Time out up");
                this.StopTimer();
            }
            else
            {
                timer += Time.deltaTime;
            }
        }
    }
}
