﻿using UnityEngine;

public class HideButtonsOnCompile : MonoBehaviour {

    private void Start()
    {
        #if UNITY_STANDALONE_WIN
            this.gameObject.SetActive(true);
        #else
            this.gameObject.SetActive(false);
        #endif
    }
}
