﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class AudioController : MonoBehaviour {

    #region Singletone and Do not destroy on load
    public static AudioController audioController;

    private void Awake()
    {
        if (audioController == null)
            audioController = this;
        else
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
    }
    #endregion

    [System.Serializable]
    public struct AudioDictionary
    {
        public string sceneName;
        public AudioClip sceneClip;
    }

    public AudioSource source;
    public List<AudioDictionary> audioList = new List<AudioDictionary>();

    private string curScene;

    private void OnLevelWasLoaded(int level)
    {
        curScene = SceneManager.GetActiveScene().name;

        foreach(AudioDictionary d in audioList)
        {
            if(d.sceneName == curScene)
            {
                source.clip = d.sceneClip;

                if (source.isPlaying)
                    return;
                else
                    source.Play();
            }
        }
    }
}
