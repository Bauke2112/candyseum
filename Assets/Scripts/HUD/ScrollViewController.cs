﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewController : MonoBehaviour {

    public Scrollbar scroll;
    [Header("Numero de Paginas da loja")]

    public int numberOfSteps;
    private float howMuchToWalk;
	
	void Awake () {
        
        scroll.value = 0f;
               
	}

    public void ChangeWindowToBeShowed(Button button)
    {
        float tempDivisor = numberOfSteps / 2f;
        howMuchToWalk = 1f / tempDivisor;
        //Debug.Log(howMuchToWalk + "      " + tempDivisor);

        if(button.CompareTag("Right"))
        {
            //Debug.Log("DIREITA");
            scroll.value += howMuchToWalk;         
        }
        else if(button.CompareTag("Left"))
        {
            //Debug.Log("ESQUERDA");
            scroll.value -= howMuchToWalk;
        }
    }
}
