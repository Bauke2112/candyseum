﻿using UnityEngine;
using TMPro;

public class InfoManager : MonoBehaviour {

    public TextMeshProUGUI goldQuant;

    private void Update()
    {
        goldQuant.text = "$" + PlayerGold.instance.goldQuant.ToString();
    }
}
