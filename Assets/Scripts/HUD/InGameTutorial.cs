﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityLib;
using DG.Tweening;

public class InGameTutorial : MonoBehaviour {

    //Tutorial ended event
    public delegate void InGameTutorialHandler();
    public event InGameTutorialHandler InGameTutorialFinished;

    [Header("Player Initial Weapons")]
    public Melee defaultMelee;
    public Ranged defaultRanged;
    public Melee nullMelee;
    public Ranged nullRanged;

    [Header("Tutorial parts")]
    public GameObject[] tutorialParts;
    [Space]
    public GameObject weaponItem;
    public Vector2 pos;
    [Space]
    public GameObject item;
    public Things healthPotion;

    private bool canStart = true;

    private bool movementPart;
    private bool movementPartEnded;

    private bool pickUpPart;
    private bool pickUpPartEnded;

    private bool weaponUsePart;
    private bool weaponUserPartEnded;

    private bool changeWeaponPart;
    private bool changeWeaponPartEnded;

    private bool bowUsePart;
    private bool bowUsePartEnded;

    private bool rechargePart;
    private bool rechargePartEnded;

    private bool useAbilityPart;
    private bool useAbilityPartEnded;

    private bool pickUpPotionPart;
    private bool pickUpPotionPartEnded;

    private bool useInventiryPart;
    private bool useInventiryPartEnded;

    private bool hasDamaged;

    private void Update()
    {
        if(canStart)
        {
            canStart = false;
            if (HUDController.inGameTutorial == true)
            {
                tutorialParts[0].SetActive(true);
                foreach (Player p in MultplayerController.instance.players)
                {
                    WeaponController wController = p.GetComponentInChildren<WeaponController>();
                    wController.melee = nullMelee;
                    wController.ranged = nullRanged;
                }
            }else
            {
                OnInGameTutorialFinished();
            }
        }

        //Move Tutorial Part
        #region Movement Part
        if(InputManager.Movement(1) != Vector3.zero && !movementPartEnded)
        {
            movementPart = true;
            movementPartEnded = true;
        }

        if(movementPart)
        {
            //Start Next Part and fade this part
            CanvasGroup c = tutorialParts[0].GetComponent<CanvasGroup>();
            StartCoroutine(DecreaseAlphaValue(c));

            tutorialParts[1].SetActive(true);
            CanvasGroup t2 = tutorialParts[1].GetComponent<CanvasGroup>();
            StartCoroutine(IncreaseAlphaValue(t2));

            if (t2.alpha == 1)
            {
                movementPart = false;
                pickUpPart = true;
                tutorialParts[0].SetActive(false);
            }
        }
        #endregion

        //Pick Up Part
        #region Pick Up Part
        if(pickUpPart)
        {
            if(!pickUpPartEnded)
            {
                //Instantiate Weapons to pick up
                pickUpPartEnded = true;
                WeaponItem m = Instantiate(weaponItem, pos, Quaternion.identity, null).GetComponent<WeaponItem>();
                WeaponItem r = Instantiate(weaponItem, -pos, Quaternion.identity, null).GetComponent<WeaponItem>();

                m.item = defaultMelee;
                r.item = defaultRanged;

                //Set pick up text active
                tutorialParts[2].SetActive(true);
                CanvasGroup c = tutorialParts[2].GetComponent<CanvasGroup>();
                StartCoroutine(IncreaseAlphaValue(c));         
            }

            //Check if Player picked up weapons
            foreach (Player p in MultplayerController.instance.players)
            {
                WeaponController wController = p.GetComponentInChildren<WeaponController>();
                if (wController.melee != nullMelee && wController.ranged != nullRanged)
                {
                    CanvasGroup c = tutorialParts[1].GetComponent<CanvasGroup>();
                    StartCoroutine(DecreaseAlphaValue(c));
                }
            }
        }

        if(tutorialParts[1].GetComponent<CanvasGroup>().alpha == 0 && pickUpPart)
        {
            tutorialParts[1].SetActive(false);
            pickUpPart = false;
            weaponUsePart = true;
        }
        #endregion

        //Weapon Use Part
        #region Use Weapon Part
        if(weaponUsePart)
        {
            if(!weaponUserPartEnded)
            {
                tutorialParts[2].SetActive(true);
                StartCoroutine(IncreaseAlphaValue(tutorialParts[2].GetComponent<CanvasGroup>()));

                if (Input.GetMouseButton(0) || InputManager.BottomRightTrigger(1) != 0)
                {
                    weaponUserPartEnded = true;
                    DOTweenAnimation tween = tutorialParts[2].GetComponent<DOTweenAnimation>();
                    tween.DOPlay();
                }
            }
        }
        #endregion

        //Chnage weapon Part
        #region Change Weapon Part
        if(changeWeaponPart)
        {
            tutorialParts[3].SetActive(true);
            StartCoroutine(IncreaseAlphaValue(tutorialParts[3].GetComponent<CanvasGroup>()));

            if(!changeWeaponPartEnded)
            {
                if(Input.GetAxisRaw("Mouse ScrollWheel") != 0 || InputManager.TopRightTrigger(1) || InputManager.TopLeftTrigger(1))
                {
                    changeWeaponPartEnded = true;
                    DOTweenAnimation tween = tutorialParts[3].GetComponent<DOTweenAnimation>();
                    tween.DOPlay();
                }
            }
        }
        #endregion

        //Bow Use Part
        #region Bow Use Part
        if(bowUsePart)
        {
            tutorialParts[4].SetActive(true);
            StartCoroutine(IncreaseAlphaValue(tutorialParts[4].GetComponent<CanvasGroup>()));

            if(!bowUsePartEnded)
            {
                if(Input.GetMouseButton(0) || InputManager.BottomRightTrigger(1) != 0)
                {
                    bowUsePartEnded = true;
                    DOTweenAnimation tween = tutorialParts[4].GetComponent<DOTweenAnimation>();
                    tween.DOPlay();
                }
            }
        }
        #endregion

        //Recharge Weapon Part
        #region Recharge Weapon Part
        if(rechargePart)
        {
            tutorialParts[5].SetActive(true);
            StartCoroutine(IncreaseAlphaValue(tutorialParts[5].GetComponent<CanvasGroup>()));

            if (!rechargePartEnded)
            {
                if(InputManager.X_Button(1) || Input.GetKeyDown(KeyCode.R))
                {
                    rechargePartEnded = true;
                    DOTweenAnimation tween = tutorialParts[5].GetComponent<DOTweenAnimation>();
                    tween.DOPlay();
                }
            }
        }
        #endregion

        //Use ability Part
        #region Ability Part
        if(useAbilityPart)
        {
            tutorialParts[6].SetActive(true);
            StartCoroutine(IncreaseAlphaValue(tutorialParts[6].GetComponent<CanvasGroup>()));

            if(!useAbilityPartEnded)
            {
                foreach (Player p in MultplayerController.instance.players)
                {
                    PlayerStats stats = p.GetComponent<PlayerStats>();
                    stats.mana = stats.maxMana;
                }

                if (Input.GetKeyDown(KeyCode.Q) || InputManager.Y_Button(1))
                {
                    useAbilityPartEnded = true;
                    DOTweenAnimation tween = tutorialParts[6].GetComponent<DOTweenAnimation>();
                    tween.DOPlay();
                }
            }
        }
        #endregion

        //Pick Up Potion
        #region Pick Up Potion
        if(pickUpPotionPart)
        {
            tutorialParts[7].SetActive(true);
            StartCoroutine(IncreaseAlphaValue(tutorialParts[7].GetComponent<CanvasGroup>()));

            if(!pickUpPotionPartEnded)
            {
                GameObject go = Instantiate(item, pos, Quaternion.identity, null);
                go.GetComponent<Item>().item = healthPotion;

                pickUpPotionPartEnded = true;
            }

            foreach(Player p in MultplayerController.instance.players)
            {
                Inventory inv = p.GetComponent<Inventory>();
                
                foreach(Slot s in inv.slots)
                {
                    if(s.thing == healthPotion)
                    {
                        DOTweenAnimation tween = tutorialParts[7].GetComponent<DOTweenAnimation>();
                        tween.DOPlay();
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
        #endregion

        //Use Inventory Part
        #region Use Inventory Part
        if(useInventiryPart)
        {
            tutorialParts[8].SetActive(true);
            StartCoroutine(IncreaseAlphaValue(tutorialParts[8].GetComponent<CanvasGroup>()));

            if(!hasDamaged)
            {
                hasDamaged = true;
                foreach(Player p in MultplayerController.instance.players)
                {
                    PlayerStats stats = p.GetComponent<PlayerStats>();
                    stats.Damage(40f);
                }
            }

            if (!useInventiryPartEnded)
            {
                if (Input.GetKeyDown(KeyCode.Alpha1) ||
                (InputManager.DPadVertical(1) == 1 ||
                (Input.GetKeyDown(KeyCode.Alpha2) ||
                (InputManager.DPadVertical(1) == -1) ||
                (Input.GetKeyDown(KeyCode.Alpha3) ||
                (InputManager.DPadHorizontal(1) == 1
                ))
                )))
                {
                    useInventiryPartEnded = true;
                    DOTweenAnimation tween = tutorialParts[8].GetComponent<DOTweenAnimation>();
                    tween.DOPlay();
                }
            }
        }
        #endregion
    }

    public void EndUseInventoryPart()
    {
        useInventiryPart = false;
        tutorialParts[8].SetActive(false);
        OnInGameTutorialFinished();
    }

    public void EndPickUpPotionPart()
    {
        pickUpPotionPart = false;
        tutorialParts[7].SetActive(false);
        useInventiryPart = true;
    }

    public void EndAbilityPart()
    {
        useAbilityPart = false;
        tutorialParts[6].SetActive(false);
        pickUpPotionPart = true;
    }

    public void EndRechargePart()
    {
        rechargePart = false;
        tutorialParts[5].SetActive(false);
        useAbilityPart = true;
    }

    public void EndBowUsePart()
    {
        bowUsePart = false;
        tutorialParts[4].SetActive(false);
        rechargePart = true;
    }

    public void EndChangeWeapon()
    {
        changeWeaponPart = false;
        tutorialParts[3].SetActive(false);
        bowUsePart = true;
    }

    public void EndPickUpPart()
    {
        weaponUsePart = false;
        tutorialParts[2].SetActive(false);
        changeWeaponPart = true;
    }

    protected virtual void OnInGameTutorialFinished()
    {
        foreach(Player p in MultplayerController.instance.players)
        {
            WeaponController wController = p.GetComponentInChildren<WeaponController>();
            wController.melee = defaultMelee;
            wController.ranged = defaultRanged;

            PlayerStats stats = p.GetComponent<PlayerStats>();
            stats.mana = 0;
            stats.hp = stats.maxHp;
        }

        if (InGameTutorialFinished != null)
            InGameTutorialFinished();

        HUDController.inGameTutorial = false;
        this.SetActive(false);
    }

    IEnumerator DecreaseAlphaValue(CanvasGroup group)
    {
        group.alpha -= Time.deltaTime;
        yield return new WaitForSeconds(.2f);
    }

    IEnumerator IncreaseAlphaValue(CanvasGroup group)
    {
        group.alpha += Time.deltaTime;
        yield return new WaitForSeconds(.2f);
    }
}
