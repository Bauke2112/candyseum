﻿using UnityEngine;

public class FadeOverTime : MonoBehaviour {

    public bool canFade;
    public float delay = 5f;
    public float speed = 2f;

    public float startAlpha = 1;

    private float startTime;
    private float timer;

    private float alphaTimer;
    private bool startFade;

    private CanvasGroup group;
    private Inventory playerInv;

    public float speedToShow = 1.2f;

    public bool canShow;
    public float showTimer;

    private void Start()
    {
        canFade = HUDController.hideInventory;

        group = this.GetComponent<CanvasGroup>();
        playerInv = this.transform.GetChild(0).GetComponent<Slot>().playerInv;
        group.alpha = startAlpha;
        timer = delay;
    }

    private void Update()
    {
        if (!canFade)
            return;

        ShowInventory();

        if(timer <= 0)
        {
            alphaTimer = speed;
            startFade = true;
        }else
        {
            timer -= Time.deltaTime;
        }

        if (startFade)
        {
            if (alphaTimer <= 0)
            {
                group.alpha = 0;
                startFade = false;
                alphaTimer = speed;
            }
            else
            {
                alphaTimer -= Time.deltaTime;
                group.alpha -= Time.deltaTime / alphaTimer;
            }
        }

        if(canShow)
        {
            if(showTimer <= 0)
            {
                canShow = false;
                group.alpha = 1;
                timer = delay;
            }
            else
            {
                showTimer -= Time.deltaTime;
                group.alpha += Time.deltaTime / showTimer;
            }
        }
    }

    void ShowInventory()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) ||
            (InputManager.DPadVertical(playerInv.gameObject.GetComponent<Player>().number) == 1 ||
            (Input.GetKeyDown(KeyCode.Alpha2) ||
            (InputManager.DPadVertical(playerInv.gameObject.GetComponent<Player>().number) == -1) ||
            (Input.GetKeyDown(KeyCode.Alpha3) ||
            (InputManager.DPadHorizontal(playerInv.gameObject.GetComponent<Player>().number) == 1
            ))
            )))
        {
            if (!canShow)
            {
                showTimer = speedToShow;
                canShow = true;
            }
        }
    }
}
