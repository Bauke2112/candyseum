﻿using TMPro;
using UnityEngine;

public class TooltipManager : MonoBehaviour {

    public static TooltipManager instance;
    private Camera mainCamera;

    [SerializeField]
    private GameObject tooltip;

    public TextMeshProUGUI tooltipDescription;
    public TextMeshProUGUI tooltipMana;
    public TextMeshProUGUI tooltipDuration;
    public RectTransform tooltipBackground;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        mainCamera = Camera.main;
        //tooltipText = tooltip.transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Update()
    {

        //Vector3 tamanhoT = new Vector3(tooltipBackground.sizeDelta.x / 2, 0, 0);
              
        Vector2 mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        //Vector2 tooltipPos = mousePos - tamanhoT;

        tooltip.transform.position = mousePos;
    }

    public void ShowTooltip(int classID)
    {
        tooltip.SetActive(true);

        tooltipDescription.text = PlayerClassManager.instance.classes[classID].ultimateDescription;
        tooltipMana.text = PlayerClassManager.instance.classes[classID].ultimateManaUsage.ToString();
        tooltipDuration.text = PlayerClassManager.instance.classes[classID].ultimateDuration.ToString() + "s";

        //float height = tooltipDescription.preferredHeight + tooltipMana.preferredHeight + tooltipDuration.preferredHeight;

        Vector2 backgroundSize = new Vector2(tooltipBackground.sizeDelta.x, 286.2f);
        tooltipBackground.sizeDelta = backgroundSize;
    }

    public void HideTooltip()
    {
        tooltip.SetActive(false);
    }
}
