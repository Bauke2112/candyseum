﻿using UnityEngine;

public class TooltipElement : MonoBehaviour {

    [TextArea]public string text;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("TooltipElement"))
        {
            if(collision.gameObject.name.Contains("Toggle1"))
                TooltipManager.instance.ShowTooltip(0);
            else if (collision.gameObject.name.Contains("Toggle2"))
                TooltipManager.instance.ShowTooltip(1);
            else if (collision.gameObject.name.Contains("Toggle3"))
                TooltipManager.instance.ShowTooltip(2);
            else if (collision.gameObject.name.Contains("Toggle4"))
                TooltipManager.instance.ShowTooltip(3);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("TooltipElement"))
        {
            TooltipManager.instance.HideTooltip();
        }
    }

    private void OnMouseEnter()
    {
        if (this.gameObject.name.Contains("Toggle1"))
            TooltipManager.instance.ShowTooltip(0);
        else if (this.gameObject.name.Contains("Toggle2"))
            TooltipManager.instance.ShowTooltip(1);
        else if (this.gameObject.name.Contains("Toggle3"))
            TooltipManager.instance.ShowTooltip(2);
        else if (this.gameObject.name.Contains("Toggle4"))
            TooltipManager.instance.ShowTooltip(3);
    }

    private void OnMouseExit()
    {
        TooltipManager.instance.HideTooltip();
    }
}
