﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour {

    public float hp, mana, stamina;

    [HideInInspector]public float moveSpeed;
    public float runSpeed = 8f;

    public PlayerType playerClass;

    [HideInInspector]public float maxHp, maxMana, maxStamina;

    private Scene curScene;

    public bool warriorAbility;
    public bool tankAbility;
    private float timer;
    public Inventory inv;

    private Animator anim;
    public bool canDamage = true;
    public float canDamageStartTimer = 5f;
    private float canDamageTimer;

    public GameObject respawnObject;

    private void Awake()
    {
        inv = this.GetComponent<Inventory>();
        anim = this.GetComponent<Animator>();
    }

    private void Start()
    {
        curScene = SceneManager.GetActiveScene();

        foreach (PlayerClass c in PlayerClassManager.instance.classes)
        {
            if(playerClass == c.type)
            {
                maxHp = c.maxHp;
                maxMana = c.maxMana;
                maxStamina = c.maxStamina;
                moveSpeed = c.moveSpeed;
                break;
            }
        }

        hp = maxHp;
        mana = 0;
        //stamina = maxStamina;
    }

    public float GetHealthFillAmount()
    {
        float t = hp / maxHp;

        return t;
    }

    public float GetManaFillAmount()
    {
        float t = mana / maxMana;

        return t;
    }

    private void Update()
    {
        RecoverMana();

        if(warriorAbility)
        {
            if (timer >= PlayerClassManager.instance.classes[0].ultimateDuration)
            {
                warriorAbility = false;
                Destroy(GameObject.Find("PlayerAbilityParticle(Clone)"));
                timer = 0f;
            }
            else
            {
                timer += Time.deltaTime;
            }
        }

        if (tankAbility)
        {
            if (timer >= PlayerClassManager.instance.classes[3].ultimateDuration)
            {
                tankAbility = false;
                Destroy(GameObject.Find("PlayerAbilityParticle(Clone)"));
                timer = 0f;
            }
            else
            {
                timer += Time.deltaTime;
            }
        }

        Die();
    }

    public void RecoverMana()
    {
        if(mana < maxMana && Time.timeScale != 0)
        {
            mana += .016f;
        }
    }

    public void reduceSpeed(float _rSpeed, float _runSpeed_, float _timeToGoBack)
    {
        Debug.Log("ta indo");
        StartCoroutine(ReducingSpeed(_rSpeed, _runSpeed_, _timeToGoBack));

    }

    private IEnumerator ReducingSpeed(float rSpeed, float _runSpeed, float timeToGoBack)
    {
       
        Debug.Log("ANormal" + timeToGoBack);
        moveSpeed = rSpeed;
        runSpeed = _runSpeed;

        yield return new WaitForSecondsRealtime(timeToGoBack);
        Debug.Log("Normal" + timeToGoBack);
        foreach (PlayerClass c in PlayerClassManager.instance.classes)
        {
            if (playerClass == c.type)
            {                  
                moveSpeed = c.moveSpeed;
                break;
            }
        }
        runSpeed = 8;
        
        yield return null;
        
    }

    public void Damage(float damage)
    {
        if (!warriorAbility || !tankAbility)
            hp -= damage;
        else if (!canDamage)
            return;

        if(tankAbility)
        {
            hp += damage;
        }
    }

    bool CheckSpecialItensInInv()
    {
        if (inv.slots[0].thing != null)
        {
            if (inv.slots[0].thing.type == ItemType.Reborn_Statue)
            {
                inv.slots[0].thing = null;
                return true;
            }
            else
            {
                if (inv.slots[1].thing != null)
                {
                    if (inv.slots[1].thing.type == ItemType.Reborn_Statue)
                    {
                        inv.slots[1].thing = null;
                        return true;
                    }
                    else
                    {
                        if (inv.slots[2].thing != null)
                        {
                            if (inv.slots[2].thing.type == ItemType.Reborn_Statue)
                            {
                                inv.slots[2].thing = null;
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    void Die()
    {
        if(hp <= 0)
        {
            if(CheckSpecialItensInInv())
            {
                canDamage = false;
                canDamageTimer = canDamageStartTimer;
                respawnObject.SetActive(true);
                anim.SetTrigger("respawn");
                this.hp = maxHp;
            }
            else
            {
                MultplayerController.instance.deadPlayers.Add(this.gameObject);
                MultplayerController.instance.players.Remove(this.gameObject.GetComponent<Player>());
                foreach (Slot s in inv.slots)
                {
                    s.isPlayerDead = true;
                }
                this.inv.gameObject.SetActive(false);
                Destroy(this.gameObject);
            }
        }
    }
}

public enum PlayerType
{
    Warrior,
    Archer,
    Mage,
    Tank
}
