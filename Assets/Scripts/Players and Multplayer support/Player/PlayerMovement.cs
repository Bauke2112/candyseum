﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 4f; //velocidade normal
    

    private float fixedSpeed;   //velocidade do player

    private bool isRunning; //usada para verificar se player está correndo
    private Rigidbody2D rb;

    private Player player;

    [Header("Sound")]
    public AudioClip walkSound;
    public AudioClip runSound;

    private AudioSource source;

    void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();
        player = this.GetComponent<Player>();
        source = this.GetComponent<AudioSource>();

        if (rb == null || player == null)
            return;
    }

    void Update()
    {
        CheckRunning();

        if (isRunning)
            fixedSpeed = this.GetComponent<PlayerStats>().runSpeed;
        else
            fixedSpeed = this.GetComponent<PlayerStats>().moveSpeed;

        PlaySoundEffects();
    }

    void FixedUpdate()
    {
        Move();
    }

    void PlaySoundEffects()
    {
        if(!isRunning)
        {
            source.clip = walkSound;

            if(rb.velocity.x != 0 || rb.velocity.y != 0)
            {
                if (!source.isPlaying)
                    source.Play();
            }else
            {
                if (source.isPlaying)
                    source.Stop();
            }
        }else
        {
            source.clip = runSound;

            if (rb.velocity.x != 0 || rb.velocity.y != 0)
            {
                if (!source.isPlaying)
                    source.Play();
            }
            else
            {
                if (source.isPlaying)
                    source.Stop();
            }
        }
    }

    void CheckRunning()
    {
            if (Input.GetKey(KeyCode.LeftShift) || InputManager.Run_Button(player.number))
                isRunning = true;
            else
                isRunning = false;
    }

    void Move()
    {
        rb.velocity = new Vector2(InputManager.Movement(player.number).x * fixedSpeed, InputManager.Movement(player.number).y * fixedSpeed);
    }
}
