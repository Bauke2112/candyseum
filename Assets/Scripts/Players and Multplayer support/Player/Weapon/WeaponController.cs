﻿using UnityEngine;

public class WeaponController : MonoBehaviour
{
    #region Variables
    [Header("Current Weapon")]
    public GameObject weapon;
    public float rotSpeed = 8f;

    [Header("Current Guns")]
    public Melee melee;
    public Ranged ranged;

    [Header("Collisions")]
    public LayerMask enemyMask;

    private SpriteRenderer weaponRend;

    public int index;
    [SerializeField] private Transform curGun;

    private bool canAttack = true;
    private float timer;

    private Animator meleeAnim;
    private Player player;

    private AudioSource source;

    [Header("Sound")]
    public AudioClip rangedSound;
    public AudioClip meleeSound;

    [Header("Weapon Pick Up")]
    public GameObject weaponItem;

    public bool isReloading;
    private float reloadTimer;

    public GameObject popUpDamageText;
    public Canvas popUpDamageCanvas;

    private PlayerStats myStats;

    private Vector3 gridx, gridy;
    private bool spotted = false;

    [SerializeField] private GameObject wallIndicator;
    private SpriteRenderer IndicatorImage;
    [SerializeField] private Sprite CannotPlaceBlockSprite;
    [SerializeField] private Sprite CanPlaceBlockSprite;

    public PlayerInfoManager infoManager;
    public Melee testSword;
    public Ranged testBow;

    private const float BASE_SCALE = 2.1f;
    #endregion

    //Events
    public delegate void WeaponPickUpHandler(Weapon newWeapon, Weapon oldWeapon);
    public event WeaponPickUpHandler WeaponPickUp;

    private void Awake()
    {
        weaponRend = curGun.GetComponent<SpriteRenderer>();
        meleeAnim = curGun.GetComponent<Animator>();
        player = GetComponentInParent<Player>();
        myStats = GetComponentInParent<PlayerStats>();
        source = GetComponent<AudioSource>();
        popUpDamageCanvas = GameObject.FindGameObjectWithTag("Effect Canvas").GetComponent<Canvas>();

        if (weaponRend == null || meleeAnim == null || player == null || popUpDamageCanvas == null || myStats == null)
            return;      
    }

    private void Start()
    {
        IndicatorImage = wallIndicator.GetComponent<SpriteRenderer>();
        ranged.curAmmo = ranged.maxAmmo;
    }

    private void Update()
    {
        ChangeIndex();
        ResetCanAttack();
        ReloadWeapon();

        if(isReloading)
        {
            if(reloadTimer >= ranged.reloadTime)
            {
                isReloading = false;
                ranged.curAmmo = ranged.maxAmmo;
            }else
            {
                reloadTimer += Time.deltaTime;
            }

            if (infoManager == null)
                return;

            infoManager.bulletImage.fillAmount = reloadTimer / ranged.reloadTime;
        }else
        {
            if (infoManager == null)
                return;

            infoManager.bulletImage.fillAmount = 1;
        }

        if (!PauseMenu.instance.isOpen)
            UseWeapon();

        if (index == 0)
        {
            weaponRend.sprite = melee.weaponSprite;
            source.clip = meleeSound;

            this.transform.localScale = new Vector3(melee.size/BASE_SCALE, melee.size/BASE_SCALE, 1f);
        }
        else if (index == 1)
        {
            weaponRend.sprite = ranged.weaponSprite;

            source.clip = rangedSound;
            this.transform.localScale = new Vector3(ranged.size/BASE_SCALE, ranged.size/BASE_SCALE, 1f);
        }

        if(Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.L))
        {
            this.melee = testSword;
            this.ranged = testBow;
        }
    }

    private void FixedUpdate()
    {
        Rotate();
    }

    #region Methods
    void OnWeaponPickUp(Weapon newWeapon, Weapon oldWeapon)
    {
        if (WeaponPickUp != null)
            WeaponPickUp(newWeapon, oldWeapon);
    }

    bool lockRotation = false;
    void UseWeapon()
    {
        if ((Input.GetMouseButton(0) || InputManager.BottomRightTrigger(player.number) > 0) && canAttack)
        {
            if (isReloading)
                isReloading = false;

            canAttack = false;
            Debug.Log("Attack!!");
            if (index == 0)
            {
                canAttack = false;
                //melee attack
                timer = melee.attackSpeed;

                if (!source.isPlaying)
                    source.Play();

                Collider2D[] hits = Physics2D.OverlapCircleAll(curGun.transform.position, melee.attackRange);

                foreach (Collider2D col in hits)
                {
                    if (col.CompareTag("Enemy"))
                    {
                        int roll = 0;
                        roll = Random.Range(0, 101);

                        if (roll < melee.criticChance)
                        {
                            if (myStats.warriorAbility)
                            {
                                float extraDamage = 0.5f * melee.criticDamage;
                                col.GetComponent<EnemySpawnAI>().Damage(melee.criticDamage + extraDamage);
                            }
                            else
                            {
                                col.GetComponent<EnemySpawnAI>().Damage(melee.criticDamage);
                            }

                            var popUp = Instantiate(popUpDamageText, col.transform.position, Quaternion.identity, popUpDamageCanvas.transform);
                            popUp.transform.localPosition = col.transform.position;
                            popUp.GetComponent<PopUpDamageText>().SetText(melee.criticDamage);
                        }
                        else
                        {
                            if (myStats.warriorAbility)
                            {
                                float extraDamage = 0.5f * melee.damage;
                                col.GetComponent<EnemySpawnAI>().Damage(melee.damage + extraDamage);
                            }
                            else
                            {
                                col.GetComponent<EnemySpawnAI>().Damage(melee.damage);
                            }

                            var popUp = Instantiate(popUpDamageText, col.transform.position, Quaternion.identity, popUpDamageCanvas.transform);
                            popUp.transform.localPosition = col.transform.position;
                            popUp.GetComponent<PopUpDamageText>().SetText(melee.damage);
                        }

                        KnockBack(-2500, col.GetComponent<Rigidbody2D>(), col.transform);
                    }
                    else if (col.CompareTag("Boss"))
                    {
                        int roll = 0;
                        roll = Random.Range(0, 101);

                        if (roll < melee.criticChance)
                        {
                            col.GetComponent<BossAI>().Damage(melee.criticDamage);

                            var popUp = Instantiate(popUpDamageText, col.transform.position, Quaternion.identity, popUpDamageCanvas.transform);
                            popUp.transform.localPosition = col.transform.position;
                            popUp.GetComponent<PopUpDamageText>().SetText(melee.criticDamage);
                        }
                        else
                        {
                            col.GetComponent<BossAI>().Damage(melee.damage);

                            var popUp = Instantiate(popUpDamageText, col.transform.position, Quaternion.identity, popUpDamageCanvas.transform);
                            popUp.transform.localPosition = col.transform.position;
                            popUp.GetComponent<PopUpDamageText>().SetText(melee.damage);
                        }

                        KnockBack(-2500, col.GetComponent<Rigidbody2D>(), col.transform);
                    }
                }
                meleeAnim.SetBool("isAttacking", true);
            }
            else if (index == 1)
            {
                if (ranged.curAmmo <= 0)
                {
                    timer = ranged.fireRate;
                    return;
                }
                else
                {
                    canAttack = false;
                    //ranged attack
                    if (ranged.name != "Wall Gun")
                        ranged.curAmmo--;

                    if (ranged.name == "Lança Chamas")
                    {
                        var dir = (MultplayerController.instance.cursors[player.number - 1].transform.position - this.transform.position).normalized;

                        GameObject bullet = Instantiate(ranged.ammo, this.transform.position + 1.3f * dir, this.transform.rotation);
                        bullet.transform.SetParent(this.transform);

                        timer = ranged.fireRate;
                        return;
                    }
                    else if (ranged.name == "Wall Gun")
                    {

                        AnexToGrid(this.transform.position.x, this.transform.position.y);
                        timer = ranged.fireRate;
                        return;
                    }
                    else
                    {
                        //bullet.GetComponent<Bullet>().dir = Vector2.right;
                        GameObject bullet = Instantiate(ranged.ammo, this.transform.position, this.transform.rotation);

                        int roll = 0;
                        roll = Random.Range(0, 101);

                        if (roll < ranged.criticChance)
                        {
                            bullet.GetComponent<Bullet>().damageValue = ranged.criticDamage;
                        }
                        else
                        {
                            bullet.GetComponent<Bullet>().damageValue = ranged.damage;
                        }
                    }

                    
                }
                timer = ranged.fireRate;
            }
        }
        else
        {
            if (index == 0)
            {
                meleeAnim.SetBool("isAttacking", false);
            }
        }

        if (index == 1)
        {
            if (ranged.name == "Wall Gun")
            {

                ShowGrid(this.transform.position.x, this.transform.position.y);
                CertifyDontHaveBlockInFront();
                
                return;
            }
            else
                wallIndicator.SetActive(false);
        }
        else
        {
            wallIndicator.SetActive(false);
        }
    }

    void AnexToGrid(float pos, float posY)
    {

        gridx = new Vector3(Mathf.RoundToInt(pos), Mathf.RoundToInt(posY), 0) + 1 * transform.right;
        gridx = new Vector3(Mathf.RoundToInt(gridx.x), Mathf.RoundToInt(gridx.y), 0);
        Debug.Log(spotted);
        if (spotted == false)
        {
            IndicatorImage.sprite = CanPlaceBlockSprite;
            ranged.curAmmo--;
            GameObject bullet = Instantiate(ranged.ammo, gridx, new Quaternion(0, 0, 0, 0));
        }
            
    }

    void ShowGrid(float posX, float posY)
    {
        wallIndicator.SetActive(true);
        gridy = new Vector3(Mathf.RoundToInt(posX), Mathf.RoundToInt(posY), 0) + 1 * transform.right;
        wallIndicator.transform.position = new Vector3(Mathf.RoundToInt(gridy.x), Mathf.RoundToInt(gridy.y), 0);
        wallIndicator.transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    void CertifyDontHaveBlockInFront()
    {
        Debug.DrawLine(this.transform.position, wallIndicator.transform.position, Color.magenta);
        spotted = Physics2D.Linecast(this.transform.position, wallIndicator.transform.position, 1 << LayerMask.NameToLayer("Obstacles"));
        if(spotted == true)                   
            IndicatorImage.sprite = CannotPlaceBlockSprite;                    
        else
            IndicatorImage.sprite = CanPlaceBlockSprite;
    }

    void ReloadWeapon()
    {
        if (Input.GetKeyDown(KeyCode.R) || InputManager.X_Button(player.number))
        {
            if (ranged.curAmmo == ranged.maxAmmo)
                return;

            isReloading = true;
            reloadTimer = 0;
            infoManager.bulletImage.fillAmount = 0;
        }
    }

    void ResetCanAttack()
    {
        if (!canAttack && timer > 0)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                canAttack = true;
                timer = 0;
            }
        }
    }

    void Rotate()
    {
        if (lockRotation) return;

        Vector3 dir = player.cursor.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotSpeed * Time.deltaTime);
    }

    void ChangeIndex()
    {
        if (Input.GetAxisRaw("Mouse ScrollWheel") > 0 || InputManager.TopRightTrigger(player.number))//InputManager.TopRightTrigger(player.number))
        {
            index++;

            if (index >= 2)
                index = 0;
        }
        else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0 || InputManager.TopLeftTrigger(player.number))//InputManager.TopLeftTrigger(player.number))
        {
            index--;

            if (index < 0)
                index = 1;
        }
    }

    public bool PickUpWeapon(Weapon newWeapon, Transform hitTransform)
    {
        if (newWeapon is Melee)
        {
            if (melee != null && (Melee)newWeapon != melee)
            {
                OnWeaponPickUp(newWeapon, melee);
                var _i = Instantiate(weaponItem, hitTransform.position, Quaternion.identity, null);
                _i.GetComponent<WeaponItem>().item = melee;

                melee = null;
                melee = (Melee)newWeapon;
                return true;
            }
            else if (melee == null)
            {
                melee = (Melee)newWeapon;
                return true;
            }
            else if ((Melee)newWeapon == melee)
            {
                return false;
            }
        }
        else if (newWeapon is Ranged)
        {
            if (ranged != null && (Ranged)newWeapon != ranged)
            {
                OnWeaponPickUp(newWeapon, ranged);
                var _i = Instantiate(weaponItem, hitTransform.position, Quaternion.identity, null);
                _i.GetComponent<WeaponItem>().item = ranged;

                ranged = null;
                ranged = (Ranged)newWeapon;
                ranged.curAmmo = ranged.maxAmmo;
                return true;
            }
            else if (ranged == null)
            {
                ranged = (Ranged)newWeapon;
                ranged.curAmmo = ranged.maxAmmo;
                return true;
            }
            else if ((Ranged)newWeapon == ranged)
            {
                return false;
            }
        }

        return false;
    }

    public void KnockBack(float knockbackPower, Rigidbody2D otherRb, Transform otherTrans)
    {
        var dir = (transform.position - otherTrans.position).normalized;

        otherRb.AddForce(dir * knockbackPower);
    }
    #endregion
}