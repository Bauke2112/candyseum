﻿using UnityEngine;

public class Bullet : MonoBehaviour {

    public float velocity = 15f;
    private readonly float timeToDestroy = 3f;

    public Vector2 dir;
    public float damageValue;

    private AudioSource source;

    public Canvas popUpDamageCanvas;
    public GameObject popUpDamageText;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
        popUpDamageCanvas = GameObject.FindGameObjectWithTag("Effect Canvas").GetComponent<Canvas>();

        if (source == null || popUpDamageCanvas == null)
            return;
    }

    private void Start()
    {
        dir = Vector2.right;
        Destroy(this.gameObject, timeToDestroy);
    }

    private void Update()
    {
        Move();
    }

    public void Move()
    {
        transform.Translate(dir * velocity * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!source.isPlaying)
            source.Play();

        if (collision.gameObject.tag == "Enemy")
        {
            KnockBack(-2500, collision.GetComponent<Rigidbody2D>(), collision.transform);

            FreezeEffect.instance.Freeze();
            collision.gameObject.GetComponent<EnemySpawnAI>().Damage(damageValue);

            var popUp = Instantiate(popUpDamageText, collision.transform.position, Quaternion.identity, popUpDamageCanvas.transform);
            popUp.transform.localPosition = collision.transform.position;
            popUp.GetComponent<PopUpDamageText>().SetText(damageValue);

            CameraShake.instance.ShakeCamera(.2f, .15f);
            Destroy(this.gameObject);          
        } else if (collision.gameObject.tag == "Boss")
        {
            KnockBack(-2500, collision.GetComponent<Rigidbody2D>(), collision.transform);

            FreezeEffect.instance.Freeze();
            collision.gameObject.GetComponent<BossAI>().Damage(damageValue);

            var popUp = Instantiate(popUpDamageText, collision.transform.position, Quaternion.identity, popUpDamageCanvas.transform);
            popUp.transform.localPosition = collision.transform.position;
            popUp.GetComponent<PopUpDamageText>().SetText(damageValue);

            CameraShake.instance.ShakeCamera(.2f, .15f);
            Destroy(this.gameObject);
        }
    }

    public void KnockBack(float knockbackPower, Rigidbody2D otherRb, Transform otherTrans)
    {
        var dir = (transform.position - otherTrans.position).normalized;

        otherRb.AddForce(dir * knockbackPower);
    }
}
