﻿using UnityEngine;

public class Wall : MonoBehaviour {

    public int maxHitTimes = 5;
    private int hits;

    public float timeToDestroy = 20f;

    private void Start()
    {
        Destroy(this.gameObject, timeToDestroy);
        hits = maxHitTimes;
    }

    private void Update()
    {
        if (hits <= 0)
            Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.GetComponent<Player>() || other.gameObject.GetComponent<BossAI>())
        {
            return;
        }else
        {
            hits -= 1;
            Spawner.instance.OnEnemyDeath();
            Instantiate(EnemySpawnAI.WalldeathEffect, this.transform.position, this.transform.rotation);
            Destroy(other.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Player>() || other.gameObject.GetComponent<BossAI>())
        {
            return;
        }
        else
        {
            hits -= 1;
            Destroy(other.gameObject);
        }
    }
}
