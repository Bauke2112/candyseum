﻿using UnityEngine;

public class CandyBullet : MonoBehaviour {

    public GameObject explosionEffect;

    private void OnDestroy()
    {
        Instantiate(explosionEffect, this.transform.position, Quaternion.identity, null);
    }
}
