﻿using UnityEngine;

public class PlayerAnimationController : MonoBehaviour {

    private Animator anim;
    private Rigidbody2D player;

    private void Awake()
    {
        anim = this.GetComponent<Animator>();
        player = this.GetComponent<Rigidbody2D>();

        if (anim == null || player == null)
            return;
    }

    private void Update()
    {
        if(player.velocity.x == 0)
            anim.SetInteger("velocity", (int)Mathf.Clamp(player.velocity.y, -1, 1));
        else if (player.velocity.y == 0)
            anim.SetInteger("velocity", (int)Mathf.Clamp(player.velocity.x, -1, 1));
    }
}
