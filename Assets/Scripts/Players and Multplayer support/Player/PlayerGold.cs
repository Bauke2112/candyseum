﻿using UnityEngine;

public class PlayerGold : MonoBehaviour {

    public static PlayerGold instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);
    }

    public int goldQuant;
}
