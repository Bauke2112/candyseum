﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer), typeof(CircleCollider2D))]
public class WeaponItem : MonoBehaviour
{
    public Weapon item;
    public float radius;

    public float timeToDestroy = 15f;

    public GameObject overlayCanvas;
    public Image arrowImage;
    public Color greenColor;
    public Color redColor;

    public string keyboardKey = "Press E to pick up";
    public string joystickKey = "Press A to pick up";

    private SpriteRenderer rend;
    private CircleCollider2D col;

    private void Awake()
    {
        rend = this.GetComponent<SpriteRenderer>();
        col = this.GetComponent<CircleCollider2D>();

        if (rend == null || col == null)
            return;
    }

    private void Start()
    {
        rend.sprite = item.weaponSprite;
        col.radius = radius;

        if (this.item.name == "Null Melee" || this.item.name == "Null Ranged")
            Destroy(this.gameObject);

        Destroy(this.gameObject, timeToDestroy);
        //InvokeRepeating("ChangeStats", 0f, 5f);
        this.transform.localScale = new Vector3(
            item.size, item.size, item.size
        );
    }

    private void Update()
    {
        Collider2D collision = Physics2D.OverlapCircle(this.transform.position, radius);

        if (collision.gameObject != null)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (Input.GetKeyDown(KeyCode.E) || InputManager.A_Button(collision.gameObject.GetComponent<Player>().number))
                {
                    var weaponHolder = collision.GetComponentInChildren<WeaponController>();
                    if (weaponHolder.PickUpWeapon(item, this.transform))
                    {
                        Destroy(this.gameObject);
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
    }

    /*private void ChangeStats()
    {
        rend.sprite = item.weaponSprite;
        col.radius = radius;
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        WeaponController wCont = collision.GetComponentInChildren<WeaponController>();

        if (collision.gameObject.tag == "Player")
        {
            if(MultplayerController.instance.connectedControllers.Count != 0)
            {
                overlayCanvas.GetComponentInChildren<TextMeshProUGUI>().text = joystickKey;
            }else
            {
                overlayCanvas.GetComponentInChildren<TextMeshProUGUI>().text = keyboardKey;
            }

            if(wCont.index == 0)
            {
                if(wCont.melee.damage < item.damage && item is Melee)
                {
                    arrowImage.color = greenColor;
                    arrowImage.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
                }else if (wCont.melee.damage >= item.damage && item is Melee)
                {
                    arrowImage.color = redColor;
                    arrowImage.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
                }else
                {
                    arrowImage.SetActive(false);
                }
            }else
            {
                if (wCont.ranged.damage < item.damage && item is Ranged)
                {
                    arrowImage.color = greenColor;
                    arrowImage.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
                }
                else if (wCont.melee.damage >= item.damage && item is Ranged)
                {
                    arrowImage.color = redColor;
                    arrowImage.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
                }
                else
                {
                    arrowImage.SetActive(false);
                }
            }

            overlayCanvas.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            overlayCanvas.SetActive(false);
        }
    }

    /*private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E) || InputManager.A_Button(collision.gameObject.GetComponent<Player>().number))
            {
                var weaponHolder = collision.GetComponentInChildren<WeaponController>();
                if(weaponHolder.PickUpWeapon(item, this.transform))
                {
                    Destroy(this.gameObject);
                }else
                {
                    return;
                }
            }
        }
    }*/
}

