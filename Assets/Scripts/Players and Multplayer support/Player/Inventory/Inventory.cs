﻿using UnityEngine;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

    public GameObject inventoryHUD;

    private GameObject pInv;

    public List<Slot> slots = new List<Slot>();
    private Vector3 pos;

    [HideInInspector] public PlayerStats playerStats;

    private void Awake()
    {
        playerStats = this.GetComponent<PlayerStats>();

        if (playerStats == null)
            return;
    }

    private void Start()
    {
        pInv = Instantiate(inventoryHUD,Vector3.zero,Quaternion.identity,null);

        pInv.name = "Inventory " + this.name;
        pInv.transform.SetParent(GameObject.FindGameObjectWithTag("HUD").transform);
        pInv.transform.localScale = new Vector3(0.47807f, 0.47807f, 0.47807f);

        var number = this.GetComponent<Player>().number;

        if(number == 1)
        {
            pInv.transform.localPosition = new Vector3(-316, 150);
        }else if(number == 2)
        {
            pInv.transform.localPosition = new Vector3(-91, 150);
        }else if(number == 3)
        {
            pInv.transform.localPosition = new Vector3(91, 150);
        }else if(number == 4)
        {
            pInv.transform.localPosition = new Vector3(316, 150);
        }

        foreach(Slot s in pInv.GetComponentsInChildren<Slot>())
        {
            s.playerInv = this;
            slots.Add(s);
        }
        MultplayerController.instance.playersInv.Add(pInv);
    }

    public bool AddItem(Things item)
    {
        if (slots[0].thing == null)
        {
            slots[0].thing = item;
            slots[0].stackQuant++;
            slots[0].itemSprite.sprite = item.image;
            return true;
        }
        else if (slots[0].thing != null)
        {
            if (slots[0].thing != item)
            {
                if (slots[1].thing == null)
                {
                    slots[1].thing = item;
                    slots[1].stackQuant++;
                    slots[1].itemSprite.sprite = item.image;
                    return true;
                }
                else if (slots[1].thing != null)
                {
                    if (slots[1].thing != item)
                    {
                        if (slots[2].thing == null)
                        {
                            slots[2].thing = item;
                            slots[2].stackQuant++;
                            slots[2].itemSprite.sprite = item.image;
                            return true;
                        }
                        else if (slots[2].thing != null)
                        {
                            if (slots[2].thing != item)
                                return false;
                            else
                            {
                                if (slots[2].stackQuant < slots[2].thing.stackMaxQuant)
                                {
                                    slots[2].stackQuant++;
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (slots[1].stackQuant < slots[1].thing.stackMaxQuant)
                        {
                            slots[1].stackQuant++;
                            return true;
                        }
                        else
                        {
                            if (slots[2].thing == null)
                            {
                                slots[2].thing = item;
                                slots[2].stackQuant++;
                                slots[2].itemSprite.sprite = item.image;
                                return true;
                            }
                            else if (slots[2].thing != null)
                            {
                                if (slots[2].thing != item)
                                    return false;
                                else
                                {
                                    if (slots[2].stackQuant < slots[2].thing.stackMaxQuant)
                                    {
                                        slots[2].stackQuant++;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (slots[0].thing == item)
            {
                if (slots[0].stackQuant < slots[0].thing.stackMaxQuant)
                {
                    slots[0].stackQuant++;
                    return true;
                }
                else
                {
                    if (slots[1].thing == null)
                    {
                        slots[1].thing = item;
                        slots[1].stackQuant++;
                        slots[1].itemSprite.sprite = item.image;
                        return true;
                    }
                    else if (slots[1].thing != null)
                    {
                        if (slots[1].thing != item)
                        {
                            if (slots[2].thing == null)
                            {
                                slots[2].thing = item;
                                slots[2].stackQuant++;
                                slots[2].itemSprite.sprite = item.image;
                                return true;
                            }
                            else if (slots[2].thing != null)
                            {
                                if (slots[2].thing != item)
                                    return false;
                                else
                                {
                                    if (slots[2].stackQuant < slots[2].thing.stackMaxQuant)
                                    {
                                        slots[2].stackQuant++;
                                        return true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (slots[1].stackQuant < slots[1].thing.stackMaxQuant)
                            {
                                slots[1].stackQuant++;
                                return true;
                            }
                            else
                            {
                                if (slots[2].thing == null)
                                {
                                    slots[2].thing = item;
                                    slots[2].stackQuant++;
                                    slots[2].itemSprite.sprite = item.image;
                                    return true;
                                }
                                else if (slots[2].thing != null)
                                {
                                    if (slots[2].thing != item)
                                        return false;
                                    else
                                    {
                                        if (slots[2].stackQuant < slots[2].thing.stackMaxQuant)
                                        {
                                            slots[2].stackQuant++;
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }
}
