﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerInfoManager : MonoBehaviour {

    public TextMeshProUGUI ammoInfo;
    public Image bulletImage;

    public WeaponController wController;

    private void Start()
    {
        wController.infoManager = this;
    }

    private void Update()
    {
        /*if(wController.isReloading)
        {
            ammoInfo.text = "Reloading...";
            ammoInfo.GetComponent<RectTransform>().sizeDelta = new Vector2(ammoInfo.preferredWidth, ammoInfo.preferredHeight);
        }
        else
        {
            ammoInfo.text = wController.ranged.curAmmo.ToString();
            ammoInfo.GetComponent<RectTransform>().sizeDelta = new Vector2(ammoInfo.preferredWidth, ammoInfo.preferredHeight);
            ammoInfo.GetComponent<RectTransform>().localPosition = new Vector3(52.5f, ammoInfo.GetComponent<RectTransform>().position.y);
        }*/
        ammoInfo.text = wController.ranged.curAmmo.ToString();
    }
}
