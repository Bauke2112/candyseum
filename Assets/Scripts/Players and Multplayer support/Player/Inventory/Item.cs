﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer),typeof(CircleCollider2D))]
public class Item : MonoBehaviour {

    public Things item;
    public float radius;

    public float timeToDestroy = 15f;

    private SpriteRenderer rend;
    private CircleCollider2D col;

    private void Awake()
    {
        rend = this.GetComponent<SpriteRenderer>();
        col = this.GetComponent<CircleCollider2D>();

        if (rend == null || col == null)
            return;
    }

    private void Start()
    {
        rend.sprite = item.image;
        col.radius = radius;

        Destroy(this.gameObject, timeToDestroy);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Inventory hitInv = collision.gameObject.GetComponent<Inventory>();
            if(hitInv.AddItem(this.item))
            {
                FadeOverTime fade = hitInv.slots[0].transform.parent.GetComponent<FadeOverTime>();
                if(!fade.canShow)
                {
                    fade.canShow = true;
                    fade.showTimer = fade.speedToShow;
                }
                Destroy(this.gameObject);
            }else
            {
                return;
            }
        }
    }
}
