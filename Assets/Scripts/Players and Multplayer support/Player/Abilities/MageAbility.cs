﻿using UnityEngine;

public class MageAbility : MonoBehaviour {

    public float duration = 30f;

    private CircleCollider2D col;

    private Collider2D[] hits;

    private void Awake()
    {
        col = this.GetComponent<CircleCollider2D>();

        if (col == null)
            return;

        duration = PlayerClassManager.instance.classes[2].ultimateDuration;
    }

    private void Start()
    {
        Destroy(GameObject.Find("PlayerAbilityParticle(Clone)"));
        Destroy(this.gameObject, duration);
    }

    private void Update()
    {
        hits = Physics2D.OverlapCircleAll(this.transform.position, col.radius);

        foreach (Collider2D c in hits)
        {
            if (c.gameObject.CompareTag("Player"))
            {
                c.GetComponent<PlayerStats>().hp += 0.16f;
            }
            else if (c.gameObject.CompareTag("Enemy"))
            {
                c.GetComponent<EnemySpawnAI>().Damage(0.16f);
            }
            else if (c.gameObject.CompareTag("Boss"))
            {
                c.GetComponent<EnemySpawnAI>().Damage(0.16f);
            }
        }
    }
}
