﻿using UnityEngine;

public class ArcherAbility : MonoBehaviour {

    private void Start()
    {
        foreach (Transform t in this.transform)
        {
            t.GetComponent<Bullet>().damageValue = 25;
        }
    }
}
