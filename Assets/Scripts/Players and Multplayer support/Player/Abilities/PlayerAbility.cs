﻿using UnityEngine;

public class PlayerAbility : MonoBehaviour {

    public GameObject archerBullet;
    public GameObject mageAbility;

    public GameObject playerAbilityEffect;

    private int number;
    private PlayerStats myStats;

    private void Awake()
    {
        number = this.GetComponent<Player>().number;
        myStats = this.GetComponent<PlayerStats>();
    }

    private void Update()
    {
        UseUltimate();
    }

    void UseUltimate()
    {
        if(Input.GetKeyDown(KeyCode.Q) || InputManager.Y_Button(number))
        {
            foreach(PlayerClass c in PlayerClassManager.instance.classes)
            {
                if(c.type == myStats.playerClass)
                {
                    if(myStats.mana >= c.ultimateManaUsage)
                    {                      
                        if(myStats.playerClass == PlayerType.Archer)
                        {
                            Instantiate(archerBullet, this.transform.position, Quaternion.identity);
                            myStats.mana -= c.ultimateManaUsage;
                        } else if (myStats.playerClass == PlayerType.Warrior)
                        {
                            Instantiate(playerAbilityEffect, this.transform);
                            myStats.warriorAbility = true;
                            myStats.mana -= c.ultimateManaUsage;
                        }else if(myStats.playerClass == PlayerType.Tank)
                        {
                            Instantiate(playerAbilityEffect, this.transform);
                            myStats.tankAbility = true;
                            myStats.mana -= c.ultimateManaUsage;
                        }
                        else if (myStats.playerClass == PlayerType.Mage)
                        {
                            Instantiate(playerAbilityEffect, this.transform);
                            Instantiate(mageAbility, this.transform.position, Quaternion.identity);
                            myStats.mana -= c.ultimateManaUsage;
                        }
                    }
                }
            }
        }
    }
}
