﻿using UnityEngine;

public class Player : MonoBehaviour {

    public int number; //numero do player
    public GameObject statsBar;
    public GameObject playerInfo;
    public Canvas canvas;

    public SpriteRenderer rend;

    [HideInInspector] public Transform cursor;

    private void Awake()
    {
        //rend = this.GetComponent<SpriteRenderer>();
        canvas = GameObject.FindGameObjectWithTag("HUD").GetComponent<Canvas>();

        if (rend == null || canvas == null)
            return;
    }

    private void Start()
    {
        if (number == 1)
            rend.color = Color.green;
        else if (number == 2)
            rend.color = Color.blue;
        else if (number == 3)
            rend.color = Color.yellow;
        else if (number == 4)
            rend.color = Color.red;

        InstatiateStatsBars();
    }

    void InstatiateStatsBars()
    {
        GameObject bar = Instantiate(statsBar, Vector3.zero, Quaternion.identity, canvas.transform);
        GameObject info = Instantiate(playerInfo, Vector3.zero, Quaternion.identity, canvas.transform);

        if (number == 1)
        {
            bar.transform.localPosition = new Vector3(-316, 198);
            info.transform.localPosition = new Vector3(-332, 110);
        }
        else if (number == 2)
        {
            bar.transform.localPosition = new Vector3(-91, 198);
            info.transform.localPosition = new Vector3(-107, 110);
        }
        else if (number == 3)
        {
            bar.transform.localPosition = new Vector3(91, 198);
            info.transform.localPosition = new Vector3(107, 110);
        }
        else if (number == 4)
        {
            bar.transform.localPosition = new Vector3(316, 198);
            info.transform.localPosition = new Vector3(332, 110);
        }

        HealthBar _bH = bar.GetComponent<HealthBar>();
        _bH.targetStats = this.GetComponent<PlayerStats>();

        ManaBar _bM = bar.GetComponent<ManaBar>();
        _bM.targetStats = this.GetComponent<PlayerStats>();

        info.GetComponent<PlayerInfoManager>().wController = this.gameObject.GetComponentInChildren<WeaponController>();
    }
}
