﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class MultplayerController : MonoBehaviour {

    #region Singletone
    public static MultplayerController instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);

        foreach(string s in connectedControllers)
        {
            connectedControllers.Remove(s);
        }
    }
    #endregion

    public bool lockJoysticks;

    public GameObject player;
    public GameObject cursor;

    public List<string> connectedControllers = new List<string>();
    public List<GameObject> playersInv = new List<GameObject>();
    public List<Player> players = new List<Player>();
    public List<GameObject> deadPlayers = new List<GameObject>();

    public List<MouseCursor> cursors = new List<MouseCursor>();

    private void Start()
    {
        if(!lockJoysticks)
            connectedControllers = Input.GetJoystickNames().ToList();

        SpawnPlayers();
    }

    private void Update()
    {
        if(players.Count == 0)
        {
            StartCoroutine(Transitions.instance.LoadScene("GameOver"));
        }
    }

    private void SpawnPlayers()
    {
        if (connectedControllers.Count == 0)
        {
            GameObject _player = Instantiate(player, Vector3.zero, Quaternion.identity);
            //GameObject _cursor = Instantiate(cursor, Vector3.zero, Quaternion.identity);
            _player.GetComponent<Player>().number = 1;
            //_cursor.GetComponent<MouseCursor>().number = 1;

            if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 0)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Warrior;
            }
            else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 1)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Archer;
            }
            else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 2)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Mage;
            }
            else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 3)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Tank;
            }

            players.Add(_player.GetComponent<Player>());
            //cursors.Add(_cursor.GetComponent<MouseCursor>());
        }
        else if (connectedControllers.Count == 1)
        {
            GameObject _player = Instantiate(player, Vector3.zero, Quaternion.identity);
            //GameObject _cursor = Instantiate(cursor, Vector3.zero, Quaternion.identity);
            _player.GetComponent<Player>().number = 1;
            //_cursor.GetComponent<MouseCursor>().number = 1;

            if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 0)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Warrior;
            }
            else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 1)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Archer;
            }
            else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 2)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Mage;
            }
            else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 3)
            {
                _player.GetComponent<PlayerStats>().playerClass = PlayerType.Tank;
            }

            players.Add(_player.GetComponent<Player>());
            //cursors.Add(_cursor.GetComponent<MouseCursor>());
        }
        else if (connectedControllers.Count == 2)
        {
            for (int i = 0; i < connectedControllers.Count; i++)
            {
                GameObject _player = Instantiate(player, Vector3.zero, Quaternion.identity);
                //GameObject _cursor = Instantiate(cursor, Vector3.zero, Quaternion.identity);
                _player.GetComponent<Player>().number = i + 1;
                //_cursor.GetComponent<MouseCursor>().number = i + 1;

                if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 0)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Warrior;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 1)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Archer;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 2)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Mage;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 3)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Tank;
                }

                players.Add(_player.GetComponent<Player>());
                //cursors.Add(_cursor.GetComponent<MouseCursor>());
            }
        }
        else if (connectedControllers.Count == 3)
        {
            for (int i = 0; i < connectedControllers.Count; i++)
            {
                GameObject _player = Instantiate(player, Vector3.zero, Quaternion.identity);
                //GameObject _cursor = Instantiate(cursor, Vector3.zero, Quaternion.identity);
                _player.GetComponent<Player>().number = i + 1;
                //_cursor.GetComponent<MouseCursor>().number = i + 1;

                if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 0)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Warrior;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 1)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Archer;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 2)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Mage;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 3)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Tank;
                }

                players.Add(_player.GetComponent<Player>());
                //cursors.Add(_cursor.GetComponent<MouseCursor>());
            }
        }
        else if (connectedControllers.Count == 4)
        {
            for (int i = 0; i < connectedControllers.Count; i++)
            {
                GameObject _player = Instantiate(player, Vector3.zero, Quaternion.identity);
                //GameObject _cursor = Instantiate(cursor, Vector3.zero, Quaternion.identity);
                _player.GetComponent<Player>().number = i + 1;
                //_cursor.GetComponent<MouseCursor>().number = i + 1;

                if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 0)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Warrior;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 1)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Archer;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 2)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Mage;
                }
                else if (PlayerPrefs.GetInt("Player" + _player.GetComponent<Player>().number) == 3)
                {
                    _player.GetComponent<PlayerStats>().playerClass = PlayerType.Tank;
                }

                players.Add(_player.GetComponent<Player>());
                //cursors.Add(_cursor.GetComponent<MouseCursor>());
            }
        }

        SpawnCursors();
    }

    private void SpawnCursors()
    {
        foreach(Player p in players)
        {
            var c = Instantiate(cursor, p.transform.position, Quaternion.identity, null);
            c.gameObject.GetComponent<MouseCursor>().number = p.number;

            p.cursor = c.transform;
            cursors.Add(c.GetComponent<MouseCursor>());
        }
    }
}