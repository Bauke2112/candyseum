﻿using UnityEngine;

public static class InputManager {
    
    //-- Axis
    public static float Horizontal(int playerNumber)
    {
        float r = 0.0f;
        r += Input.GetAxisRaw("J_Horizontal" + playerNumber);
        r += Input.GetAxisRaw("Horizontal");

        return r;
    }

    public static float Vertical(int playerNumber)
    {
        float r = 0.0f;
        r += Input.GetAxisRaw("J_Vertical" + playerNumber);
        r += Input.GetAxisRaw("Vertical");

        return r;
    }

    public static Vector3 Movement(int playerNumber)
    {
        return new Vector3(Horizontal(playerNumber), Vertical(playerNumber), 0.0f).normalized;
    }

    public static int DPadHorizontal(int playerNumber)
    {
        float r = 0.0f;
        r += Input.GetAxisRaw("DPad_Horizontal_" + playerNumber);

        if(r > .75)
        {
            return 1;
        }else if(r < -.75)
        {
            return -1;
        }

        return 0;
    }

    public static int DPadVertical(int playerNumber)
    {
        float r = 0.0f;
        r += Input.GetAxisRaw("DPad_Vertical_" + playerNumber);

        if (r > .75)
        {
            return 1;
        }
        else if (r < -.75)
        {
            return -1;
        }

        return 0;
    }

    //-- Second Axis
    public static float SecondHorizontal(int playerNumber)
    {
        float x = 0f;

        x += Input.GetAxisRaw("J_Horizontal_" + playerNumber);

        return x;
    }

    public static float SecondVertical(int playerNumber)
    {
        float y = 0f;

        y += Input.GetAxisRaw("J_Vertical_" + playerNumber);

        return y;
    }

    //-- Triggers
    public static float BottomRightTrigger(int number)
    {
        float x = 0f;

        x += Input.GetAxisRaw("BR_Trigger" + number);

        return x;
    }

    public static float BottomLeftTrigger(int number)
    {
        float x = 0f;

        x += Input.GetAxisRaw("BL_Trigger" + number);

        return x;
    }

    public static bool TopRightTrigger(int playerNumber)
    {
        return Input.GetButton("TR_Trigger_" + playerNumber);
    }

    public static bool TopLeftTrigger(int playerNumber)
    {
        return Input.GetButton("TL_Trigger_" + playerNumber);
    }

    //-- Buttons

    public static bool A_Button(int playerNumber)
    {
        return Input.GetButton("A_Button" + playerNumber);
    }

    public static bool B_Button(int playerNumber)
    {
        return Input.GetButton("B_Button" + playerNumber);
    }

    public static bool X_Button(int playerNumber)
    {
        return Input.GetButton("X_Button" + playerNumber);
    }

    public static bool Y_Button(int playerNumber)
    {
        return Input.GetButton("Y_Button" + playerNumber);
    }

    public static bool Run_Button(int playerNumber)
    {
        return Input.GetButton("Run_Button_" + playerNumber);
    }

    public static bool Start_Button()
    {
        return Input.GetButton("Start_Button");
    }

    public static bool GUI_B_Button()
    {
        return Input.GetButton("GUI_B_Button");
    }
}
