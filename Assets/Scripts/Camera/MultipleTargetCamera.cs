﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MultipleTargetCamera : MonoBehaviour {

    public List<Player> targets;

    public Vector3 offset;
    public float smoothTime = .5f;

    private Vector3 velocity;

    private Camera cam;

    public float minZoom = 40f;
    public float maxZoom = 5f;
    public float zoomLimeter = 50f;

    private Transform holder;

    private void Start()
    {
        cam = GetComponent<Camera>();
        holder = this.transform.parent;
    }

    private void Update()
    {
        holder.position = new Vector3(transform.position.x, transform.position.y, -10f);
        targets = MultplayerController.instance.players;   
    }

    void LateUpdate()
    {     
        if (targets.Count == 0)
            return;
        
        Move();
        Zoom();
    }

    void Move()
    {
        Vector3 centerPoint = GetCenterPoint();

        Vector3 newPosition = centerPoint + offset;

        holder.position = Vector3.SmoothDamp(holder.position, newPosition, ref velocity, smoothTime);
    }

    void Zoom()
    {
        float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance() / zoomLimeter);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, newZoom, Time.deltaTime);
    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(targets[0].transform.position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].transform.position);
        }

        return bounds.size.x;
    }

    Vector3 GetCenterPoint()
    {
        if(targets.Count == 1)
        {
            return targets[0].transform.position;
        }

        var bounds = new Bounds(targets[0].transform.position, Vector3.zero);
        for(int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].transform.position);
        }

        return bounds.center;
    }
}
