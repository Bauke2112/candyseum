﻿using UnityEngine;
using System.Collections;

public class Spikes : MonoBehaviour {

    public float damage = 5f;
    public float timeBetweenDamage = 1.5f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StartCoroutine(Damage(collision));
        }
    }

    private IEnumerator Damage(Collider2D collision)
    {
        PlayerStats stats = collision.GetComponent<PlayerStats>();
        stats.Damage(damage);
        yield return new WaitForSeconds(timeBetweenDamage);
    }
}
