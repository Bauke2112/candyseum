﻿using Timers;
using UnityEngine;

public class Bomb : MonoBehaviour {

    public float timeToExplode = 3f;
    public float explosionRadius = .3f;
    public float damage = 5f;
    public GameObject explosionEffect;

    private bool canDamage = false;

    private void Start()
    {
        Timer.CreateTimer(Explode, timeToExplode);
    }

    private void Explode()
    {
        canDamage = true;
        Collider2D[] hits = Physics2D.OverlapCircleAll(this.transform.position, explosionRadius);

        foreach(Collider2D col in hits)
        {
            if(col.GetComponent<EnemySpawnAI>())
            {
                col.GetComponent<EnemySpawnAI>().hp -= damage;
            }
            else if(col.GetComponent<BossAI>())
            {
                col.GetComponent<BossAI>().hp -= damage;
            }
        }

        Instantiate(explosionEffect, this.transform.position, Quaternion.identity, null);
        Destroy(this.gameObject);
    }
}
