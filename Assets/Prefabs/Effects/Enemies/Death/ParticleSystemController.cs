﻿using UnityEngine;

public class ParticleSystemController : MonoBehaviour {

    public float duration;
    private float timer;

    private void Start()
    {
        duration = this.GetComponent<ParticleSystem>().main.duration + this.GetComponent<ParticleSystem>().main.startLifetimeMultiplier;
        timer = duration;
    }

    private void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
            Destroy(this.gameObject);
    }
}
