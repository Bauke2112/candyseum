﻿using UnityEngine;

[CreateAssetMenu(fileName = "Item",menuName = "Item/Item",order = 1)]
public class Things : ScriptableObject {

    public new string name;
    public int stackMaxQuant = 16;
    public float useValue = 5;
    public ItemType type;

    public int price = 10;

    public Sprite image;
}

public enum ItemType
{
    Potion_HP,
    Poison_HP,
    Potion_Mana,
    Poison_Mana,
    Reborn_Statue,
    Bomb,
    Cherry,
    Ice_Cream,
    Donut
}
