﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Timers;

public class Slot : MonoBehaviour {

    public int slotNumber;
    public Things thing;

    public int stackQuant;

    public Image itemSprite;
    public TextMeshProUGUI quantText;

    public bool isPlayerDead;

    [HideInInspector]public Inventory playerInv;
    public GameObject bombObject;

    private void Awake()
    {
        if (itemSprite == null || quantText == null)
            return;
    }

    private void Start()
    {
        quantText.text = "0";
    }

    private void Update()
    {
        if (thing == null)
        {
            stackQuant = 0;
            quantText.text = "0";
            itemSprite.sprite = null;
            itemSprite.color = new Color(itemSprite.color.r, itemSprite.color.g, itemSprite.color.b, 0f);
            return;
        }else
        {
            itemSprite.color = new Color(itemSprite.color.r, itemSprite.color.g, itemSprite.color.b, 255f);
        }

        quantText.text = stackQuant.ToString();

        #region Código comentado
        /*if ((Input.GetKeyDown(KeyCode.Alpha1) || InputManager.DPadHorizontal(playerInv.gameObject.GetComponent<Player>().number) > 0) && slotNumber == 1)
        {
            if (stackQuant - 1 < 0)
                return;

            if(stackQuant - 1 <= 0)
            {
                thing = null;
                return;
            }

            UseItem(thing.useValue,thing.type);
        }
        else if ((Input.GetKeyDown(KeyCode.Alpha2) || InputManager.DPadVertical(playerInv.gameObject.GetComponent<Player>().number) > 0) && slotNumber == 2)
        {         
            if (stackQuant - 1 < 0)
                return;

            if (stackQuant - 1 <= 0)
            {
                thing = null;
                return;
            }

            UseItem(thing.useValue, thing.type);
        }
        else if ((Input.GetKeyDown(KeyCode.Alpha3) || InputManager.DPadHorizontal(playerInv.gameObject.GetComponent<Player>().number) < 0) && slotNumber == 3)
        {
            if (stackQuant - 1 < 0)
                return;

            if (stackQuant - 1 <= 0)
            {
                thing = null;
                return;
            }

            UseItem(thing.useValue, thing.type);
        }
        */
        #endregion
        UseSlotItem();

        if (isPlayerDead)
            return;

        if (InputManager.DPadHorizontal(playerInv.gameObject.GetComponent<Player>().number) == 0)
            hPressed = false;

        if (InputManager.DPadVertical(playerInv.gameObject.GetComponent<Player>().number) == 0)
            vPressed = false;
    }

    bool hPressed = false;
    bool vPressed = false;

    public void UseSlotItem()
    {
        if (isPlayerDead)
            return;

        if (Input.GetKeyDown(KeyCode.Alpha1) ||
            (InputManager.DPadVertical(playerInv.gameObject.GetComponent<Player>().number) == 1
            && !vPressed))
        {
            vPressed = true;
            if (this.slotNumber != 1)
                return;
            else
            {
                if (this.stackQuant > 1)
                {
                    UseItem(this.thing.useValue, this.thing.type);
                }
                else if (this.stackQuant == 1)
                {
                    UseItem(this.thing.useValue, this.thing.type);
                    this.thing = null;
                }
                else if (this.stackQuant == 0)
                {
                    return;
                }
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) ||
                 (InputManager.DPadVertical(playerInv.gameObject.GetComponent<Player>().number) == -1
                 && !vPressed))
        {
            vPressed = true;
            if (this.slotNumber != 2)
                return;
            else
            {
                if (this.stackQuant > 1)
                {
                    UseItem(this.thing.useValue, this.thing.type);
                }
                else if (this.stackQuant == 1)
                {
                    UseItem(this.thing.useValue, this.thing.type);
                    this.thing = null;
                }
                else if (this.stackQuant == 0)
                {
                    return;
                }
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) ||
                 (InputManager.DPadHorizontal(playerInv.gameObject.GetComponent<Player>().number) == 1
                 && !hPressed))
        {
            hPressed = true;
            if (this.slotNumber != 3)
                return;
            else
            {
                if (this.stackQuant > 1)
                {
                    UseItem(this.thing.useValue, this.thing.type);
                }
                else if (this.stackQuant == 1)
                {
                    UseItem(this.thing.useValue, this.thing.type);
                    this.thing = null;
                }
                else if (this.stackQuant == 0)
                {
                    return;
                }
            }
        }
    }

    public void UseItem(float useValue, ItemType type)
    {
        if (type == ItemType.Potion_HP)
        {
            if (playerInv.playerStats.hp == playerInv.playerStats.maxHp)
                return;

            if (playerInv.playerStats.hp + useValue > playerInv.playerStats.maxHp)
            {
                playerInv.playerStats.hp += playerInv.playerStats.maxHp - playerInv.playerStats.hp;
            }
            else
            {
                playerInv.playerStats.hp += useValue;
            }

            stackQuant--;
        }
        else if (type == ItemType.Potion_Mana)
        {
            if (playerInv.playerStats.mana == playerInv.playerStats.maxMana)
                return;

            if (playerInv.playerStats.mana + useValue > playerInv.playerStats.maxMana)
            {
                playerInv.playerStats.mana += playerInv.playerStats.maxMana - playerInv.playerStats.mana;
            }
            else
            {
                playerInv.playerStats.mana += useValue;
            }

            stackQuant--;
        }
        else if(type == ItemType.Bomb)
        {
            //Bomb code
            Instantiate(bombObject, playerInv.transform.position, Quaternion.identity, null);
            stackQuant--;
        }else if(type == ItemType.Ice_Cream)
        {
            //Ice Cream code
        }else if(type == ItemType.Cherry)
        {
            //Cherry Code
        }
        else if (type == ItemType.Donut)
        {
            //Donut Code
        }
    }
}
